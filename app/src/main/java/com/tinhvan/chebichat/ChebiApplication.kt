package com.tinhvan.chebichat

import android.app.Application
import com.tinhvan.chebichat.core.speechtotext.ChebiSpeechRecognizer
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.startKoin

class ChebiApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(appModule))
    }
}