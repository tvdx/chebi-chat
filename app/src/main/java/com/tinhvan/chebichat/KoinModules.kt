package com.tinhvan.chebichat

import com.tinhvan.chebichat.core.hanziwriter.HanziWriterApiService
import com.tinhvan.chebichat.core.speechtotext.BaiduSpeechRecognizer
import com.tinhvan.chebichat.core.speechtotext.ChebiSpeechRecognizer
import com.tinhvan.chebichat.core.speechtotext.NuanceApiSpeechRecognizer
import com.tinhvan.chebichat.core.speechtotext.SpeechRecognizer
import com.tinhvan.chebichat.core.wakeup.BaiduWakeUp
import com.tinhvan.chebichat.core.wakeup.WakeUp
import com.tinhvan.chebichat.settings.LocalSettings
import com.tinhvan.chebichat.settings.SettingsDataSource
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {
    single { RxJava2CallAdapterFactory.create() as CallAdapter.Factory }
    single { GsonConverterFactory.create() as Converter.Factory }
    single("httpLogging") { HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY) as Interceptor }
    single { OkHttpClient.Builder().addInterceptor(get("httpLogging")).build() }
    single { LocalSettings(get()) as SettingsDataSource }
    single("baidu") { BaiduSpeechRecognizer(get()) as SpeechRecognizer }
    single("nuance") { NuanceApiSpeechRecognizer(get()) as SpeechRecognizer }
    single("chebi") { ChebiSpeechRecognizer(get("baidu"), get("nuance"), get()) as SpeechRecognizer }
    single {
        Retrofit.Builder()
                .addCallAdapterFactory(get())
                .addConverterFactory(get())
                .client(get())
                .baseUrl("http://hanzi.chebichat.com/api/hanzi-writer/")
                .build()
                .create(HanziWriterApiService::class.java)
    }
    single { BaiduWakeUp(get()) as WakeUp }
}

val speechToTextModule = module {

}