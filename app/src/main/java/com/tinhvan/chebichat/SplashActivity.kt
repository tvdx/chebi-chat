package com.tinhvan.chebichat

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener
import com.tinhvan.chebichat.botchat.MainActivity
import com.tinhvan.chebichat.core.texttospeech.OfflineResource
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.Timed
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.concurrent.TimeUnit

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if (!checkStoragePermission()) {
            requestStoragePermission()
        } else {
            loadData()
        }

    }

    private fun loadData() {
        val timer = PublishSubject.create<Long>()
        Observable.timer(2, TimeUnit.SECONDS).subscribe(timer)
        Observable.fromArray("M", "F", "X", "Y")
                .subscribeOn(Schedulers.io())
                .map {
                    if (!checkStoragePermission()) {
                        return@map "No permission"
                    }
                    Log.d("NguyenNK", "Initializing voice codename: $it")
                    OfflineResource(this, it)
                    it
                }
                .takeLast(1)
                .timeInterval()
                .zipWith(timer,
                        BiFunction { t1: Timed<String>, _: Long -> t1 }
                )
                .subscribe {
                    Intent(this, MainActivity::class.java).apply {
                        startActivity(this)
                        finish()
                    }
                }
    }

    private fun checkStoragePermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false
        }
        return true
    }

    private fun requestStoragePermission() {
        val listener = SnackbarOnAnyDeniedMultiplePermissionsListener.Builder.with(view_main, getString(R.string.permission_storage_denied))
                .build()
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        listener.onPermissionsChecked(report)
                        if (report?.areAllPermissionsGranted() == true) {
                            loadData()
                        } else {
                            Intent(this@SplashActivity, MainActivity::class.java).apply {
                                startActivity(this)
                                finish()
                            }
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        listener.onPermissionRationaleShouldBeShown(permissions, token)
                    }
                })
                .onSameThread()
                .check()
    }
}
