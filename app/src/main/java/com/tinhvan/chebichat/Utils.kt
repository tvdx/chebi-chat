package com.tinhvan.chebichat

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.view.MotionEvent
import android.view.View
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import net.sourceforge.pinyin4j.PinyinHelper
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination
import java.io.IOException
import java.util.*


class Utils {
    companion object {
        private val hanyuPinyinOutputFormat = HanyuPinyinOutputFormat().apply {
            caseType = HanyuPinyinCaseType.LOWERCASE
            toneType = HanyuPinyinToneType.WITH_TONE_MARK
            vCharType = HanyuPinyinVCharType.WITH_U_UNICODE
        }

        fun isCJK(str: String): Boolean {
            val length = str.length
            for (i in 0 until length) {
                val ch = str[i]
                if (isCJK(ch)) {
                    return true
                }
            }
            return false
        }

        fun strip(str: String, keepCJK: Boolean): String {
            val sb = StringBuilder()
            for (c in str) {
                if (keepCJK == isCJK(c)) {
                    sb.append(c)
                }
            }
            return sb.toString()
        }

        private fun isCJK(ch: Char): Boolean {
            val block = Character.UnicodeBlock.of(ch)
            return (Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS == block ||
                    Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS == block ||
                    Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A == block ||
                    Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION == block)
        }

        fun touchWithinBounds(event: MotionEvent?, view: View?): Boolean {
            if (event == null || view == null || view.width == 0 || view.height == 0)
                return false

            val viewLocation = IntArray(2)
            view.getLocationOnScreen(viewLocation)
            val viewMaxX = viewLocation[0] + view.width - 1
            val viewMaxY = viewLocation[1] + view.height - 1
            return (event.rawX <= viewMaxX && event.rawX >= viewLocation[0]
                    && event.rawY <= viewMaxY && event.rawY >= viewLocation[1])
        }

        fun toPinyin(str: String): String {
            if (str.isNullOrEmpty()) {
                return str
            }

            val sb = StringBuilder()
            for (c in str.toCharArray()) {
                sb.append(toPinyin(c)).append(" ")
            }
            return sb.toString()
        }

        fun toPinyin(c: Char): String {
            try {
                val result = PinyinHelper.toHanyuPinyinStringArray(c,
                        hanyuPinyinOutputFormat)
                if (result != null) {
                    return result[0]
                }
            } catch (badHanyuPinyinOutputFormatCombination: BadHanyuPinyinOutputFormatCombination) {
                badHanyuPinyinOutputFormatCombination.printStackTrace()
            }
            return ""
        }

        fun getDeviceId(context: Context): String? {
            try {
                val adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context)

                return adInfo.id
            } catch (exception: IOException) {
                // Unrecoverable error connecting to Google Play services (e.g.,
                // the old version of the service doesn't support getting AdvertisingId).

            } catch (exception: GooglePlayServicesAvailabilityException) {
                // Encountered a recoverable error connecting to Google Play services.

            } catch (exception: GooglePlayServicesNotAvailableException) {
                // Google Play services is not available entirely.
            }
            return null
        }

        fun getLocaleStringResource(requestedLocale: Locale, resourceId: Int, context: Context): String {
            val result: String
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) { // use latest api
                val config = Configuration(context.resources.configuration)
                config.setLocale(requestedLocale)
                result = context.createConfigurationContext(config).getText(resourceId).toString()
            } else { // support older android versions
                val resources = context.resources
                val conf = resources.configuration
                val savedLocale = conf.locale
                conf.locale = requestedLocale
                resources.updateConfiguration(conf, null)

                // retrieve resources from desired locale
                result = resources.getString(resourceId)

                // restore original locale
                conf.locale = savedLocale
                resources.updateConfiguration(conf, null)
            }

            return result
        }
    }


}
