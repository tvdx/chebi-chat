package com.tinhvan.chebichat.botchat

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.tinhvan.chebichat.GlideRequests
import com.tinhvan.chebichat.R
import com.tinhvan.chebichat.core.bot.Item
import com.tinhvan.chebichat.core.bot.LyricItem
import com.tinhvan.chebichat.core.bot.VideoItem
import com.tinhvan.chebichat.core.bot.WikiItem
import com.tinhvan.chebichat.core.maps.PlaceItem
import com.tinhvan.chebichat.viewholder.*
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_image_message.view.*
import kotlinx.android.synthetic.main.item_list_message.*
import kotlinx.android.synthetic.main.item_list_message.view.*
import kotlinx.android.synthetic.main.item_lyric_message.*
import kotlinx.android.synthetic.main.item_place_message.*
import kotlinx.android.synthetic.main.item_stroke_message.*
import kotlinx.android.synthetic.main.item_text_message_me.view.*
import kotlinx.android.synthetic.main.item_video_message.*
import kotlinx.android.synthetic.main.item_video_message.view.*
import kotlinx.android.synthetic.main.item_wiki_message.*
import java.util.*

class ChatAdapter(private val glide: GlideRequests,
                  private val chatMessageList: MutableList<ChatMessage> = ArrayList()) : RecyclerView.Adapter<BaseViewHolder<*>>() {
    val videoItemClick = PublishSubject.create<VideoItem>()!!
    val btnNextClick = PublishSubject.create<BtnNextClickEvent>()!!
    val btnTranslateClick = PublishSubject.create<Pair<Int, TextChatMessage>>()!!
    val btnSpeakClick = PublishSubject.create<Pair<Int, ChatMessage>>()!!
    val btnShareClick = PublishSubject.create<ChatMessage>()!!
    val imageClick = PublishSubject.create<List<String>>()!!
    val subItemClick = PublishSubject.create<Item>()!!
    val itemIconClick = PublishSubject.create<Item>()!!
    val urlClick = PublishSubject.create<String>()!!
    val wikiItemDetailsClick = PublishSubject.create<WikiItem>()!!
    val wikiItemSpeakClick = PublishSubject.create<Pair<WikiItem, (WikiItem) -> Unit>>()!!
    val wikiItemTranslateClick = PublishSubject.create<Pair<WikiItem, (WikiItem, String) -> Unit>>()!!
    val strokeItemClick = PublishSubject.create<String>()!!
    val placeItemClick = PublishSubject.create<PlaceItem>()!!
    val placeOpenMapClick = PublishSubject.create<List<PlaceItem>>()!!
    val lyricItemTitleClick = PublishSubject.create<Pair<LyricItem, (LyricItem) -> Unit>>()!!

    private val viewPoolMap = mapOf<Class<out Any>, RecyclerView.RecycledViewPool>(
            ListItemViewHolder::class.java to RecyclerView.RecycledViewPool().apply { setMaxRecycledViews(0, 50) },
            ListVideoViewHolder::class.java to RecyclerView.RecycledViewPool().apply { setMaxRecycledViews(0, 50) },
            ListWikiViewHolder::class.java to RecyclerView.RecycledViewPool().apply { setMaxRecycledViews(0, 50) },
            ListPlaceViewHolder::class.java to RecyclerView.RecycledViewPool().apply { setMaxRecycledViews(0, 50) },
            ListLyricViewHolder::class.java to RecyclerView.RecycledViewPool().apply { setMaxRecycledViews(0, 50) }
    )

    override fun getItemViewType(position: Int): Int {
        val item = chatMessageList[position]
        return when (item) {
            is TextChatMessage -> if (item.mine) 0 else 1
            is TranslatedTextChatMessage -> if (item.mine) 2 else 3
            is ListVideoChatMessage -> 4
            is ImageChatMessage -> 5
            is AdChatMessage -> 6
            is StrokeChatMessage -> 7
            is ListGeneralChatMessage -> 8
            is UrlChatMessage -> 9
            is ListLyricChatMessage -> 12
            is ListChatMessage -> {
                when (item.getType()) {
                    VideoItem::class.java -> 4
                    Item::class.java -> 8
                    WikiItem::class.java -> 10
                    PlaceItem::class.java -> 11
                    else -> -1
                }
            }
            is TypingMessage -> 13
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val resId: Int
        val view: View
        return when (viewType) {
            0 -> {
                resId = R.layout.item_text_message_me
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                TextViewHolder(view)
            }
            1 -> {
                resId = R.layout.item_text_message_other
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                TextViewHolder(view)
            }
            2 -> {
                resId = R.layout.item_text_message_me
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                TranslatedTextViewHolder(view)
            }
            3 -> {
                resId = R.layout.item_text_message_other
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                TranslatedTextViewHolder(view)
            }
            4 -> {
                resId = R.layout.item_video_message
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                ListVideoViewHolder(view).also {
                    it.videoItemClick.subscribe(videoItemClick)
                    it.rvVideoList.recycledViewPool = viewPoolMap[this::class.java]
                }
            }
            5 -> {
                resId = R.layout.item_image_message
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                ImageViewHolder(view)
            }
            6 -> {
                resId = R.layout.item_fb_ad_message
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                AdViewHolder(view)
            }
            7 -> {
                resId = R.layout.item_stroke_message
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                StrokeViewHolder(view).also {
                    it.rvImages.recycledViewPool = viewPoolMap[this::class.java]
                    it.itemClick.subscribe(strokeItemClick)
                }
            }
            8 -> {
                resId = R.layout.item_list_message
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                ListItemViewHolder(view).also {
                    it.newsItemClick.subscribe(subItemClick)
                    it.newsIconClick.subscribe(itemIconClick)
                    it.rvNews.recycledViewPool = viewPoolMap[it::class.java]
                }
            }
            9 -> {
                resId = R.layout.item_url_message
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                UrlViewHolder(view)
            }
            10 -> {
                resId = R.layout.item_wiki_message
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                ListWikiViewHolder(view).also {
                    it.itemDetailsClick.subscribe(wikiItemDetailsClick)
                    it.itemSpeakClick.subscribe(wikiItemSpeakClick)
                    it.itemTranslateClick.subscribe(wikiItemTranslateClick)
                    it.rvWikiArticles.recycledViewPool = viewPoolMap[it::class.java]
                }
            }
            11 -> {
                resId = R.layout.item_place_message
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                ListPlaceViewHolder(view).also {
                    it.itemClick.subscribe(placeItemClick)
                    it.rvPlaces.recycledViewPool = viewPoolMap[it::class.java]
                }
            }
            12 -> {
                resId = R.layout.item_lyric_message
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                ListLyricViewHolder(view).also {
                    it.lyricItemTitleClick.subscribe(lyricItemTitleClick)
                    it.rvLyrics.recycledViewPool = viewPoolMap[it::class.java]
                }
            }
            13 -> {
                resId = R.layout.item_typing_message
                view = LayoutInflater.from(parent.context).inflate(resId, parent, false)
                TypingViewHolder(view)
            }
            else -> {
                throw Exception("Not implemented viewType")
            }
        }
    }

    override fun getItemCount(): Int {
        return chatMessageList.size
    }

    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        val item = chatMessageList[position]
        holder as BaseViewHolder<ChatMessage>
        holder.bindData(position, item)
        when (holder) {
            is ListVideoViewHolder -> {
                holder.itemView.btnNextVideos.clicks()
                        .map {
                            BtnNextClickEvent(view = holder.itemView,
                                    position = position,
                                    listChatMessage = item as ListChatMessage)
                        }
                        .subscribe(this.btnNextClick)
            }
            is TextViewHolder -> {
                holder.itemView.btnTranslate.clicks()
                        .map {
                            chatMessageList
                                    .forEachIndexed { index, item ->
                                        if ((item as? TextChatMessage)?.translated == TranslateStatus.TRANSLATING) {
                                            chatMessageList[index] = item.copy(translated = TranslateStatus.NON_TRANSLATED)
                                            notifyItemChanged(index, "TRANSLATE")
                                        }
                                    }
                            Pair(position, item as TextChatMessage)
                        }
                        .subscribe(btnTranslateClick)
                holder.itemView.btnSpeak.clicks()
                        .map {
                            chatMessageList.forEachIndexed { index, chatMessage ->
                                if ((chatMessage as? TextChatMessage)?.loadingSpeak == true) {
                                    chatMessageList[index] = chatMessage.copy(loadingSpeak = false)
                                    notifyItemChanged(index, "SPEAK")
                                }
                                if ((chatMessage as? TranslatedTextChatMessage)?.loadingSpeak == true) {
                                    chatMessageList[index] = chatMessage.copy(loadingSpeak = false)
                                    notifyItemChanged(index, "SPEAK")
                                }
                            }
                            Pair(position, item)
                        }
                        .subscribe(btnSpeakClick)
                holder.itemView.btnShare?.apply {
                    clicks().map { item }.subscribe(btnShareClick)
                }
            }
            is TranslatedTextViewHolder -> {
                holder.itemView.btnSpeak.clicks()
                        .map {
                            item as TranslatedTextChatMessage
                            Pair(position, item)
                        }
                        .subscribe(btnSpeakClick)
                holder.itemView.btnShare?.apply {
                    clicks().map { item }.subscribe(btnShareClick)
                }
            }
            is ImageViewHolder -> {
                holder.itemView.btnShare.clicks()
                        .map { item }
                        .subscribe(btnShareClick)
                holder.itemView.clicks().map { Collections.singletonList(item.message) }.subscribe(imageClick)
            }
            is StrokeViewHolder -> {
            }
            is ListItemViewHolder -> {
                holder.itemView.btnNextItems.clicks().map {
                    BtnNextClickEvent(holder.itemView, position, item as ListChatMessage)
                }
                        .subscribe(btnNextClick)
            }
            is UrlViewHolder -> {
                holder.itemView.clicks().map {
                    (item as UrlChatMessage).url
                }.subscribe(urlClick)
            }
            is ListWikiViewHolder -> {
            }
            is ListPlaceViewHolder -> {
                holder.btnOpenMap.clicks()
                        .map { (item as ListChatMessage).items as List<PlaceItem> }
                        .subscribe(placeOpenMapClick)
            }
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
            return
        }
        val item = chatMessageList[position]
        when (holder) {
            is TextViewHolder -> {
                holder.bindData(item as TextChatMessage, payloads)
            }
            is TranslatedTextViewHolder -> {
                holder.bindData(item as TranslatedTextChatMessage, payloads)
            }
            is ListLyricViewHolder -> {
                holder.bindData(position, item as ListLyricChatMessage, payloads)
            }
        }
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        videoItemClick.onComplete()
        btnNextClick.onComplete()
        btnTranslateClick.onComplete()
    }

    fun addChatMessage(position: Int = itemCount, item: ChatMessage) {
        if (item is TypingMessage && chatMessageList.contains(item)) {
            return
        }
        if (item !is TypingMessage)  {
            chatMessageList.withIndex().filter { it.value is TypingMessage }.forEach {
                chatMessageList.removeAt(it.index)
                notifyItemRemoved(it.index)
            }
        }
        if (position > itemCount - 1) {
            chatMessageList.add(item)
            notifyItemInserted(itemCount - 1)
        } else {
            chatMessageList.add(position, item)
            notifyItemInserted(position)
            notifyItemRangeChanged(position + 1, itemCount)
        }
    }

    fun updateChatMessage(position: Int, item: ChatMessage, payloads: String? = null) {
        chatMessageList[position] = item
        if (payloads != null) {
            notifyItemChanged(position, payloads)
        } else {
            notifyItemChanged(position)
        }
    }

    fun pinyinSettingsChanged(): Consumer<Boolean> {
        return Consumer { showPinyin: Boolean ->
            chatMessageList.forEachIndexed { index, chatMessage ->
                when (chatMessage) {
                    is TextChatMessage -> chatMessageList[index] = chatMessage.copy(showPinyin = showPinyin)
                    is TranslatedTextChatMessage -> chatMessageList[index] = chatMessage.copy(showPinyin = showPinyin)
                    is ListLyricChatMessage -> chatMessageList[index] = chatMessage.copy(showPinyin = showPinyin)
                }
                notifyItemChanged(index, "PINYIN")
            }
        }
    }

    data class BtnNextClickEvent(val view: View,
                                 val position: Int,
                                 val listChatMessage: ListChatMessage)
}