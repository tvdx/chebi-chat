package com.tinhvan.chebichat.botchat

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_SEND
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v4.content.PermissionChecker
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.facebook.ads.Ad
import com.facebook.ads.AdError
import com.facebook.ads.NativeAd
import com.facebook.ads.NativeAdListener
import com.facebook.drawee.backends.pipeline.Fresco
import com.firebase.ui.auth.AuthUI
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.youtube.player.YouTubeStandalonePlayer
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.touches
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener
import com.tinhvan.chebichat.GlideApp
import com.tinhvan.chebichat.R
import com.tinhvan.chebichat.Utils
import com.tinhvan.chebichat.core.authenticate.User
import com.tinhvan.chebichat.core.bot.ChebiBot
import com.tinhvan.chebichat.core.bot.ImageItem
import com.tinhvan.chebichat.core.maps.PlaceItem
import com.tinhvan.chebichat.core.speechtotext.SpeechRecognizer
import com.tinhvan.chebichat.core.wakeup.WakeUp
import com.tinhvan.chebichat.details.DetailsActivity
import com.tinhvan.chebichat.gallery.GalleryFragment
import com.tinhvan.chebichat.maps.MapsActivity
import com.tinhvan.chebichat.settings.SettingsDataSource
import com.tinhvan.chebichat.settings.SettingsDialogFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.action_bar.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.koin.android.ext.android.inject
import rxfirebase2.database.setsValue
import java.io.File
import java.io.FileOutputStream
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {
    private val chebiBot: ChebiBot by lazy {
        ChebiBot(this)
    }
    private val chatAdapter: ChatAdapter by lazy {
        val result = ChatAdapter(GlideApp.with(this))
        result
    }
    private val speechRecognizer: SpeechRecognizer by inject("chebi")

    private val disposables: MutableList<Disposable> = ArrayList()
    private var audioLevelSubscriber: Disposable? = null
    private var shouldRecord = false
    private var language = "vie-VNM"
    private val auth: FirebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }
    private val settings: SettingsDataSource by inject()
    private val RC_SIGN_IN = 1
    private var locationCheckShown = false
    private val databaseRef: FirebaseDatabase by lazy {
        FirebaseDatabase.getInstance()
    }
    private val fusedLocationProviderClient by lazy {
        FusedLocationProviderClient(this)
    }
    private val wakeUp: WakeUp by inject()
    private var recordDisposable: ArrayList<Disposable> = ArrayList()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            RC_SIGN_IN -> {
                auth.currentUser?.let {
                    val locale = Locale(settings.getNuanceLanguage().map {
                        when (it) {
                            "yue-CHN" -> "zh"
                            "eng-USA" -> "en"
                            else -> "vi"
                        }
                    }.blockingFirst())
                    val message = String.format(Utils.getLocaleStringResource(locale, R.string.hello, this), it.displayName
                            ?: "")
                    chatAdapter.addChatMessage(item = TextChatMessage(false, message))

                    Observable.fromCallable {
                        databaseRef.getReference("users_2")
                                .child(it.uid)
                                .setsValue(
                                        User(it.uid,
                                                it.displayName,
                                                it.photoUrl.toString(),
                                                it.providerId,
                                                it.providerData?.getOrNull(0)?.uid,
                                                null,
                                                Utils.getDeviceId(this)))
                                .subscribe()
                    }.subscribeOn(Schedulers.io()).subscribe()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_main)
        setUpToolbar()
        setUpAdapter()

        val llm = LinearLayoutManager(this)
        rvChat.layoutManager = llm
        rvChat.adapter = chatAdapter
        rvChat.addOnLayoutChangeListener { view, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            if (bottom < oldBottom && chatAdapter.itemCount > 0) {
                rvChat.smoothScrollToPosition(chatAdapter.itemCount - 1)
            }
        }

        setUpListeners()
        setUpSpeechRecognize()
        setUpWakeUp()

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PermissionChecker.PERMISSION_GRANTED) {
            requestLocationPermission()
        }
        if (auth.currentUser != null) {
            auth.currentUser?.let { user ->
                val locale = Locale(settings.getNuanceLanguage().map {
                    when (it) {
                        "yue-CHN" -> "zh"
                        "eng-USA" -> "en"
                        else -> "vi"
                    }
                }.blockingFirst())
                val message = String.format(Utils.getLocaleStringResource(locale, R.string.hello, this), user.displayName
                        ?: "")
                chatAdapter.addChatMessage(item = TextChatMessage(false, message))
                databaseRef.getReference("users_2").child(user.uid).setsValue(User(user.uid, user.displayName, user.photoUrl.toString(), user.providerId, user.providerData?.getOrNull(0)?.uid)).subscribe()

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PermissionChecker.PERMISSION_GRANTED) {
                    fusedLocationProviderClient.lastLocation.addOnCompleteListener {
                        it.result?.let {
                            Observable
                                    .fromCallable {
                                        val uid = Utils.getDeviceId(this)
                                                ?: user.uid
                                        databaseRef.getReference("location").child(uid).runTransaction(object : Transaction.Handler {
                                            override fun onComplete(p0: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {
                                                Log.d("NguyenNK", "Transaction onComplete: $p1")
                                            }

                                            override fun doTransaction(p0: MutableData): Transaction.Result {
                                                val map = mapOf(
                                                        "uid" to user.uid,
                                                        "latitude" to it.latitude,
                                                        "longitude" to it.longitude
                                                )
                                                p0.child("${p0.childrenCount}").value = map
                                                return Transaction.success(p0)
                                            }
                                        })
                                    }
                                    .subscribeOn(Schedulers.io())
                                    .subscribe()
                        }
                    }
                }
            }
        } else {
            authUiSignIn()
        }

        MobileAds.initialize(this, "ca-app-pub-5139530126770446~8229310558")
        Fresco.initialize(this)
    }

    private fun startWakeup() {
        recordDisposable.forEach { it.dispose() }
        recordDisposable.clear()
        btnToggleVoice.isEnabled = true
        btnToggleVoice.alpha = 1f

        recordAfterWakeup(wakeUp.onWakeUp
                .debounce(500, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .flatMap {
                    shouldRecord = true
                    startRecording()
                })

        wakeUp.start()
    }

    private fun setUpWakeUp() {
        settings.getHandFree().subscribe {
            if (it) {
//                btnToggleVoice.visibility = View.GONE
                startWakeup()
                edtInput.visibility = View.VISIBLE
                btnHoldToSpeak.visibility = View.INVISIBLE
                btnToggleVoice.setImageResource(R.mipmap.ic_mic)
            } else {
                wakeUp.cancel()
                recordDisposable.forEach { it.dispose() }
                recordDisposable.clear()

                overlayRecord.visibility = View.GONE
                speechRecognizer.cancel().subscribe()
                btnToggleVoice.visibility = View.VISIBLE
                btnToggleVoice.isEnabled = true
                btnToggleVoice.alpha = 1f
            }
        }

    }

    private fun recordAfterWakeup(onStarted: Observable<SpeechRecognizer.Event>) {
        val audioLevel = onStarted
                .switchMap {
                    Log.d("NguyenNK", "Started recording")
                    speechRecognizer.audioLevel()
                }
                .share()

        val obs1 = audioLevel
                .filter {
                    Log.d("NguyenNK", "Filter 1 volume: $it")
                    it > 60.0f
                }.debounce(1, TimeUnit.SECONDS, AndroidSchedulers.mainThread()).map { "STOP" }
        val obs2 = audioLevel
                .filter {
                    Log.d("NguyenNK", "Filter 2 volume: $it")
                    it == 0f || it > 60.0f
                }.debounce(5, TimeUnit.SECONDS, AndroidSchedulers.mainThread()).map { "CANCEL" }
        val merged = Observable.merge(obs2, obs1).take(1).share()
        val stopObs = merged.filter { it == "STOP" }
                .flatMap { stopRecording().subscribeOn(AndroidSchedulers.mainThread()).defaultIfEmpty(SpeechRecognizer.Recognition(emptyList(), "")) }
                .share()
        recordDisposable.add(stopObs
                .map {
                    it.bestResult == "" && it.resultsRecognition.isEmpty()
                }
                .subscribe { it: Boolean ->
                    Log.d("NguyenNK", "is empty: $it")
                    if (it) {
                        startWakeup()
                    }
                })

        val stopObsNotEmpty = stopObs
                .filter {
                    it.bestResult != "" && it.resultsRecognition.isNotEmpty()
                }
                .flatMap {
                    chebiBot.sendChatMessage(it.bestResult)
                            .subscribeOn(Schedulers.io())
                }
                .doOnError {
                    startWakeup()
                }
                .doOnNext {
                    Log.d("NguyenNK", "stopObsNotEmpty ChatMessage: $it")
                }
                .publish()

        recordDisposable.add(
                stopObsNotEmpty
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(displayChatMessage(), Consumer {
                            Log.d("NguyenNK", "Error: $it")
                        }))
        recordDisposable.add(
                stopObsNotEmpty.filter { it !is TranslatedTextChatMessage && it !is TextChatMessage }
                        .delay(1, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                        .subscribe({}, {}, { startWakeup() }))
        recordDisposable.add(
                stopObsNotEmpty
                        .filter {
                            (it is TranslatedTextChatMessage && !it.mine
                                    || it is TextChatMessage && !it.mine)
                        }
                        .map {
                            when (it) {
                                is TranslatedTextChatMessage -> Pair(it.textMessage, it.lang)
                                is TextChatMessage -> Pair(it.textMessage, it.lang)
                                else -> throw RuntimeException("Unexpected")
                            }
                        }.flatMap {
                            chebiBot.speak(it.first, it.second)
                        }.observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            shouldRecord = true
                            recordAfterWakeup(startRecording())
                        }, {}))
        stopObsNotEmpty.connect()
        recordDisposable.add(
                merged.filter { it == "CANCEL" }
                        .flatMap { speechRecognizer.cancel().subscribeOn(AndroidSchedulers.mainThread()) }
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            overlayRecord.visibility = View.GONE
                            startWakeup()
                        }, {
                            Log.e("NguyenNK", "Error cancel", it)
                            startWakeup()
                        }))
//        audioLevel.connect()
    }

    private fun checkLocationEnable() {
        if (locationCheckShown) {
            return
        }
        locationCheckShown = true

        val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gpsEnabled = false
        var networkEnabled = false

        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (ex: Exception) {
        }

        try {
            networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (ex: Exception) {
        }

        if (!gpsEnabled && !networkEnabled) {
            // notify user
            val dialog = AlertDialog.Builder(this)
            dialog.setMessage(getString(R.string.dialog_gps_message))
            dialog.setPositiveButton(getString(R.string.dialog_gps_positive_label)) { _, _ ->
                val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                this@MainActivity.startActivity(myIntent)
            }
            dialog.setNegativeButton(getString(R.string.dialog_gps_negative_label)) { _, _ -> }
            dialog.show()
        }
    }

    private fun displayChatMessage(): Consumer<ChatMessage> {
        return Consumer {
            Log.d("NguyenNK", "Display chat message: $it")
            if (it !is AdChatMessage) {
                chatAdapter.addChatMessage(item = it)
                rvChat.scrollToPosition(chatAdapter.itemCount - 1)

                if (it is ListChatMessage && it.typeOfItem == PlaceItem::class.java) {
                    checkLocationEnable()
                }

            } else {
                if (it.nativeAd == null) {
                    NativeAd(this, getString(R.string.fb_native_ad_id)).apply {
                        //                    NativeAd(this, "YOUR_PLACEMENT_ID").apply {
                        setAdListener(object : NativeAdListener {
                            override fun onAdClicked(p0: Ad?) {
                                Log.d("NguyenNK", "Ad clicked $p0")
                            }

                            override fun onMediaDownloaded(p0: Ad?) {
                                Log.d("NguyenNK", "Media downloaded  ${p0.toString()}")
                            }

                            override fun onError(p0: Ad?, p1: AdError?) {
                                Log.d("NguyenNK", "Ad error: ${p1?.errorMessage}")
                            }

                            override fun onAdLoaded(p0: Ad?) {
                                Log.d("NguyenNK", "Ad loaded ${p0.toString()}")
                                chatAdapter.addChatMessage(item = AdChatMessage(this@apply))
                            }

                            override fun onLoggingImpression(p0: Ad?) {
                                Log.d("NguyenNK", "Logging impression ${p0?.toString()}")
                            }
                        })
                        loadAd()
                    }
                } else {
                    chatAdapter.addChatMessage(item = it)
                }
            }
        }
    }

    private fun setUpListeners() {
        btnSend.clicks()
                .filter {
                    !edtInput.text.isNullOrBlank()
                }
                .flatMap {
                    val message = edtInput.text.toString()
                    edtInput.setText("")
                    chebiBot.sendChatMessage(message)
                            .subscribeOn(Schedulers.io())
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(displayChatMessage())

        val btnToggleVoiceClick = btnToggleVoice.clicks().share()
        btnToggleVoiceClick
                .filter { settings.getHandFree().blockingFirst() }
                .flatMap { Observable.fromCallable { wakeUp.cancel() } }
                .delay(500, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe {
                    //Test
                    recordDisposable.forEach { it.dispose() }
                    recordDisposable.clear()

                    shouldRecord = true
                    recordAfterWakeup(startRecording())
//                        wakeUp.onWakeUp.onNext(WakeUpResult())
                    btnToggleVoice.alpha = 0.5f
                    btnToggleVoice.isEnabled = false
                }
        btnToggleVoiceClick.filter { !settings.getHandFree().blockingFirst() }
                .subscribe {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                            || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestAudioPermission()
                    } else {
                        if (edtInput.visibility == View.VISIBLE) {
                            edtInput.visibility = View.INVISIBLE
                            btnHoldToSpeak.visibility = View.VISIBLE
                            btnToggleVoice.setImageResource(R.mipmap.ic_keyboard)
                        } else {
                            edtInput.visibility = View.VISIBLE
                            btnHoldToSpeak.visibility = View.INVISIBLE
                            btnToggleVoice.setImageResource(R.mipmap.ic_mic)
                        }
                    }
                }
        disposables.add(settings.getNuanceLanguage().subscribe {
            language = it
        })
        btnHoldToSpeak.touches()
                .subscribe { motionEvent ->
                    when (motionEvent.action) {
                        MotionEvent.ACTION_DOWN -> {
                            recordDisposable.add(startRecording().subscribe())
                        }
                        MotionEvent.ACTION_MOVE -> {
                            shouldRecord = Utils.touchWithinBounds(motionEvent, btnHoldToSpeak)
                        }
                        MotionEvent.ACTION_UP -> {
                            recordDisposable.add(stopRecording()
                                    .filter { shouldRecord }
                                    .flatMap {
                                        chebiBot.sendChatMessage(it.bestResult)
                                                .subscribeOn(Schedulers.io())
                                    }
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(displayChatMessage(), Consumer {}))
                        }
                        MotionEvent.ACTION_CANCEL -> {
                            recordDisposable.forEach { it.dispose() }
                            recordDisposable.clear()
                        }
                    }
                }
    }

    private fun stopRecording(): Observable<SpeechRecognizer.Recognition> {
        return speechRecognizer.stopRecording().doOnSubscribe {
            overlayRecord.visibility = View.GONE
            audioLevelSubscriber?.dispose()
        }
    }

    private fun startRecording(): Observable<SpeechRecognizer.Event> {
        return speechRecognizer.startRecording()
                .doOnSubscribe {
                    overlayRecord.visibility = View.VISIBLE
                    audioLevelSubscriber = speechRecognizer.audioLevel().subscribe {
                        if (shouldRecord) {
                            volumeDisplay.setVolume(it)
                        } else {
                            volumeDisplay.setVolume(0.0f)
                        }
                    }
                }
    }

    private fun setUpSpeechRecognize() {
//        speechRecognizer.onStartRecording().subscribe({
//            overlayRecord.visibility = View.VISIBLE
//            audioLevelSubscriber = speechRecognizer.audioLevel().subscribe {
//                if (shouldRecord) {
//                    volumeDisplay.setVolume(it)
//                } else {
//                    volumeDisplay.setVolume(0.0f)
//                }
//            }
//        }, {
//            Log.e("NguyenNK", "Error starting recording", it)
//        })
//        speechRecognizer.onFinishedRecording().subscribe({
//            overlayRecord.visibility = View.GONE
//            audioLevelSubscriber?.dispose()
//        }, {
//            Log.e("NguyenNK", "Error finish recording", it)
//        })
//        speechRecognizer.onRecognized().filter { shouldRecord }
//                .flatMap {
//                    chebiBot.sendChatMessage(it.bestResult)
//                            .subscribeOn(Schedulers.io())
//                }
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(displayChatMessage(),
//                        Consumer {
//                        })
    }

    private fun setUpAdapter() {
        chatAdapter.apply {
            videoItemClick.subscribe {
                val intent = YouTubeStandalonePlayer.createVideoIntent(this@MainActivity, "AIzaSyBomHlfrJOmP412RlodYI1UNBs27cP20_E", it.id)
                startActivity(intent)
            }
            btnNextClick
                    .switchMap {event ->
                        val listChatMessage =  event.listChatMessage
                        val input = listChatMessage.message
                        val nextPage = listChatMessage.nextPage
                        chatAdapter.addChatMessage(event.position + 1, TypingMessage())
                        chebiBot.sendChatMessage(input, nextPage)
                                .filter { it is ListChatMessage }
                                .map {
                                    Triple(event.position, ListChatMessage(listChatMessage.typeOfItem, listChatMessage.input, listChatMessage.items, null), it)
                                }
                                .subscribeOn(Schedulers.io())
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        chatAdapter.updateChatMessage(it.first, it.second)
                        chatAdapter.updateChatMessage(it.first + 1, it.third)
                    }
            btnTranslateClick
                    .flatMap { event ->
                        chatAdapter.updateChatMessage(event.first, event.second.copy(translated = TranslateStatus.TRANSLATING), "TRANSLATE")
                        chebiBot.translate(event.second)
                                .subscribeOn(Schedulers.io())
                                .doOnError { _ ->
                                    chatAdapter.updateChatMessage(event.first, event.second.copy(translated = TranslateStatus.NON_TRANSLATED), "TRANSLATE")
                                }
                                .map {
                                    Triple(event.first, event.second, it)
                                }
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        chatAdapter.updateChatMessage(it.first, it.second.copy(translated = TranslateStatus.TRANSLATED), "TRANSLATE")
                        chatAdapter.addChatMessage(it.first + 1, it.third)
                    }, {
                        Log.e("NguyenNK", "Translate error: ${it.message}", it)
                    })
            btnSpeakClick
                    .filter {
                        (!Utils.isCJK(it.second.message) || checkSpeakPermission()) &&
                                (it.second is TextChatMessage || it.second is TranslatedTextChatMessage)
                    }
                    .switchMap { event ->
                        val position = event.first
                        val chatMessage = event.second
                        val observable = when (chatMessage) {
                            is TextChatMessage -> {
                                chatAdapter.updateChatMessage(position, chatMessage.copy(loadingSpeak = true), "SPEAK")
                                chebiBot.speak(chatMessage.textMessage, chatMessage.lang)
                            }
                            is TranslatedTextChatMessage -> {
                                chatAdapter.updateChatMessage(position, chatMessage.copy(loadingSpeak = true), "SPEAK")
                                chebiBot.speak(chatMessage.textMessage, chatMessage.lang)
                            }
                            else -> throw Exception("Unexpected")
                        }
                        observable.map {
                            Log.d("NguyenNK", "Speak result: $it")
                            event
                        }
                    }
                    .onErrorResumeNext { t: Throwable -> Observable.empty() }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ event ->
                        val position = event.first
                        val chatMessage = event.second
                        when (chatMessage) {
                            is TextChatMessage -> chatAdapter.updateChatMessage(position, chatMessage.copy(loadingSpeak = false), "SPEAK")
                            is TranslatedTextChatMessage -> chatAdapter.updateChatMessage(position, chatMessage.copy(loadingSpeak = false), "SPEAK")
                        }
                    }, {
                        Log.e("NguyenNK", "OnError called")
                    }, {
                        Log.d("NguyenNK", "OnCompleted called")
                    })

            btnShareClick.subscribe {
                when (it) {
                    is ImageChatMessage -> {
                        GlideApp.with(this@MainActivity)
                                .asBitmap()
                                .load(it.source)
                                .into(object : SimpleTarget<Bitmap>() {
                                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                        val file = File(externalCacheDir, "tmp.png")
                                        val fOut = FileOutputStream(file)
                                        resource.compress(Bitmap.CompressFormat.PNG, 100, fOut)
                                        fOut.flush()
                                        fOut.close()
                                        startActivity(Intent().apply {
                                            action = ACTION_SEND
                                            putExtra(Intent.EXTRA_SUBJECT, "Từ https://www.chebichat.com")
                                            putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(applicationContext, "com.tinhvan.chebichat.provider", file))
                                            putExtra(Intent.EXTRA_TEXT, "Từ https://www.chebichat.com)")
                                            type = "*/*"
                                        })
                                    }
                                })

                    }
                    else -> {
                        startActivity(Intent().apply {
                            action = ACTION_SEND
                            putExtra(Intent.EXTRA_TEXT, "${it.message}\n\n( Từ https://www.chebichat.com)")
                            type = "text/plain"
                        })
                    }
                }
            }
            Observable.merge(placeItemClick.map { arrayListOf(it) }, placeOpenMapClick)
                    .subscribe {
                        MapsActivity.newIntent(this@MainActivity, it).let {
                            startActivity(it)
                        }
                    }
            Observable.merge(imageClick.map { item -> item[0] }, itemIconClick.map { item -> item.icon!! })
                    .map { url ->
                        chebiBot.images.indexOfFirst { it.url == url }
                    }.filter { it > -1 }
                    .subscribe { GalleryFragment.newInstance(chebiBot.images, it).show(supportFragmentManager, "tag") }
            strokeItemClick
                    .filter { chebiBot.strokes.containsKey(it) }
                    .map { url ->
                        val strokes = chebiBot.strokes[url]!!
                        Pair(strokes, strokes.indexOfFirst { it.url == url })
                    }
                    .filter { it.second > -1 }
                    .subscribe {
                        GalleryFragment.newInstance(it.first, it.second).show(supportFragmentManager, "tag")
                    }

            subItemClick
                    .subscribe {
                        if (it is ImageItem) {
                            GalleryFragment.newInstance(chebiBot.images, chebiBot.images.indexOfFirst { imageInfo -> imageInfo.url == it.icon!! }).show(supportFragmentManager, "tag")
                        } else {
                            DetailsActivity.newIntent(this@MainActivity, it.detailUrl!!).also {
                                startActivity(it)
                            }
                        }
                    }
            urlClick
                    .subscribe {
                        DetailsActivity.newIntent(this@MainActivity, it).also {
                            startActivity(it)
                        }
                    }
            wikiItemDetailsClick
                    .subscribe {
                        DetailsActivity.newIntent(this@MainActivity, it.detailUrl).also {
                            startActivity(it)
                        }
                    }
            wikiItemSpeakClick
                    .switchMap { event ->
                        val wikiItem = event.first
                        val callback = event.second
                        callback(wikiItem.copy(loadingSpeak = true))

                        chebiBot.speak(wikiItem.description, wikiItem.lang)
                                .onErrorReturnItem("IGNORED")
                                .subscribeOn(Schedulers.io())
                                .map { event }
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        it.second(it.first)
                    }
            wikiItemTranslateClick
                    .switchMap {
                        val wikiItem = it.first
                        val callback = it.second
                        if (wikiItem.translatedDesc == null) {
                            callback(wikiItem.copy(loadingTranslate = true), "TRANSLATE_STATUS")
                            chebiBot.translate(TextChatMessage(false, wikiItem.description, lang = wikiItem.lang))
                                    .subscribeOn(Schedulers.io())
                                    .map { res ->
                                        it.copy(first = wikiItem.copy(translatedDesc = res.textMessage))
                                    }
                        } else {
                            Observable.just(it)
                        }
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        it.second(it.first, "TRANSLATE")
                    }, {})
            lyricItemTitleClick
                    .flatMap {
                        val lyricItem = it.first
                        val callback = it.second

                        callback(lyricItem.copy(loading = true))
                        chebiBot.getLyricDetails(lyricItem.trackId).subscribeOn(Schedulers.io())
                                .map { Pair(lyricItem.copy(lyric = it.first, pinyinDisplay = it.second), callback) }
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        val callback = it.second
                        callback(it.first)
                    }, {})
            settings.getPinyin().subscribe(this.pinyinSettingsChanged())
        }
    }

    private fun checkSpeakPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.MODIFY_AUDIO_SETTINGS) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_STATE) != PackageManager.PERMISSION_GRANTED) {
            requestSpeakPermission()
            return false
        }
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        chebiBot.shutdown()
        disposables.forEach {
            if (!it.isDisposed)
                it.dispose()
        }
    }

    private fun requestLocationPermission() {
        val listener = SnackbarOnAnyDeniedMultiplePermissionsListener.Builder.with(view_main, getString(R.string.permission_location_denied))
                .build()
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        listener.onPermissionsChecked(report)
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
//                        token?.cancelPermissionRequest()
                        listener.onPermissionRationaleShouldBeShown(permissions, token)
//                        token?.continuePermissionRequest()
                    }
                })
                .onSameThread()
                .check()
    }

    private fun requestSpeakPermission() {
        val listener = SnackbarOnAnyDeniedMultiplePermissionsListener.Builder.with(view_main, getString(R.string.permission_speak_denied))
                .build()
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.MODIFY_AUDIO_SETTINGS,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.CHANGE_WIFI_STATE)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        listener.onPermissionsChecked(report)
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        listener.onPermissionRationaleShouldBeShown(permissions, token)
                    }
                })
                .onSameThread()
                .check()
    }

    private fun requestAudioPermission() {
        val listener = SnackbarOnAnyDeniedMultiplePermissionsListener.Builder.with(view_main, getString(R.string.permission_audio_denied))
                .build()
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(listener)
                .onSameThread()
                .check()
    }

    private fun setUpToolbar() {
        supportActionBar?.let { actionBar ->
            val actionBarLayout = layoutInflater.inflate(R.layout.action_bar, null) as ViewGroup
            actionBar.setCustomView(actionBarLayout, ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
            actionBar.setDisplayShowCustomEnabled(true)

            btnSettings.clicks().subscribe {
                val dialog = SettingsDialogFragment()
                dialog.show(supportFragmentManager, "setting-dialog")
                dialog.btnSignInClick
                        .subscribe {
                            dialog.dismiss()
                            if (auth.currentUser?.isAnonymous == false) {
                                auth.signOut()
                                authUiSignIn()
                            } else {
                                authUiSignIn()
                            }
                        }
            }
        }
    }

    private fun authUiSignIn() {
        // Choose authentication providers
        val providers = mutableListOf(
                AuthUI.IdpConfig.GoogleBuilder().build(),
                AuthUI.IdpConfig.FacebookBuilder().build())
        if (auth.currentUser == null) {
            providers.add(AuthUI.IdpConfig.AnonymousBuilder().build())
        }

        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setTheme(R.style.LoginTheme)
                        .setLogo(R.mipmap.ic_avatar)
                        .build(),
                RC_SIGN_IN)
    }
}
