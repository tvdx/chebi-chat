package com.tinhvan.chebichat.core

import android.net.Uri
import com.facebook.ads.NativeAd
import com.tinhvan.chebichat.core.bot.Item
import com.tinhvan.chebichat.core.bot.LyricItem
import com.tinhvan.chebichat.core.bot.VideoItem

enum class TranslateStatus {
    NON_TRANSLATED, TRANSLATING, TRANSLATED
}

sealed class ChatMessage(open val message: String) {
    open val dateTime = System.currentTimeMillis()
}
data class TextChatMessage(val mine: Boolean = true,
                           val textMessage: String,
                           val pinyin: String? = null,
                           var showingActions: Boolean = false,
                           var translated: TranslateStatus = TranslateStatus.NON_TRANSLATED,
                           val lang: String = "vn",
                           val avatar: Uri? = null,
                           val loadingSpeak: Boolean = false,
                           val showPinyin: Boolean = true,
                           val displayText: String? = null,
                           val senderName: String? = null,
                           override val dateTime: Long = 0) : ChatMessage(textMessage) {
}

data class TranslatedTextChatMessage(val mine: Boolean = true,
                                     val textMessage: String,
                                     val pinyin: String? = null,
                                     var showingActions: Boolean = false,
                                     val lang: String = "zh",
                                     val loadingSpeak: Boolean = false,
                                     val showPinyin: Boolean = true,
                                     val displayText: String? = null) : ChatMessage(textMessage) {
    companion object {
        fun from(original: TextChatMessage): TranslatedTextChatMessage {
            return TranslatedTextChatMessage(original.mine, original.textMessage, showingActions = original.showingActions)
        }
    }
}

data class ImageChatMessage(val source: String) : ChatMessage(source)
data class AdChatMessage(val nativeAd: NativeAd? = null) : ChatMessage("Ad from facebook")
data class StrokeChatMessage(val response: String,
                             val urls: List<String>) : ChatMessage(response)

data class UrlChatMessage(val text: String, val url: String) : ChatMessage(text)

open class ListChatMessage(val typeOfItem: Class<out Any>, open val input: String, open val items: List<Any>, open val nextPage: String?) : ChatMessage(input) {
    open fun getType(): Class<out Any> {
        return typeOfItem
    }

}

data class ListVideoChatMessage(override val items: List<VideoItem>,
                                override val input: String,
                                override val nextPage: String?) : ListChatMessage(VideoItem::class.java, input, items, nextPage)

data class ListGeneralChatMessage(override val input: String,
                                  override val items: List<Item>,
                                  override val nextPage: String? = null) : ListChatMessage(Item::class.java, input, items, nextPage)

data class ListLyricChatMessage(override val items: List<LyricItem>,
                                override val input: String,
                                val showPinyin: Boolean = true) : ListChatMessage(LyricItem::class.java, input, items, null)
data class TypingMessage(override val message: String = "") : ChatMessage(message)
