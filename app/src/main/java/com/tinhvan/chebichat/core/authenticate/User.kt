package com.tinhvan.chebichat.core.authenticate

import android.support.annotation.Keep
import com.google.android.gms.maps.model.LatLng
import java.io.Serializable

@Keep
class User : Serializable {
    constructor(uid: String?,
                displayName: String?,
                photoURL: String?,
                providerId: String? = null,
                providerUid: String? = null,
                location: LatLng? = null,
                deviceId: String? = null) {
        this.uid = uid
        this.displayName = displayName
        this.photoURL = photoURL
        this.providerId = providerId
        this.providerUid = providerUid
        this.location = location
        this.deviceId = deviceId
    }

    var uid: String? = null
    var displayName: String? = null
    var photoURL: String? = null
    var providerId: String? = null
    var providerUid: String? = null
    var location: LatLng? = null
    var deviceId: String? = null
}
