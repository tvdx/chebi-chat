package com.tinhvan.chebichat.core.bot

import com.google.gson.JsonObject
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query

interface ChebiApiService {
    @GET("/chebi/chebi_chat.php")
    fun sendMessage(@Query("lc") language: String,
                    @Query("ft") ft: Int = 0,
                    @Query("normalProb") normalProb: Int = 10,
                    @Query("reqText") reqText: String,
                    @Query("pageid") pageId: String?,
                    @Query("nextPage") nextPage: String?,
                    @Query("devMode") devMode: Boolean = false) : Observable<ChebiResponse>

    @GET("/chebi/google_translate.php")
    fun translate(@Query("content") content: String,
                  @Query("lang") lang: String,
                  @Query("target") target: String,
                  @Query("devMode") devMode: Boolean = false) : Observable<JsonObject>

    @GET("/textToVoice.php")
    fun speak(@Query("text") text: String,
              @Query("lang") lang: String,
              @Query("per") per: Int,
              @Query("spd") spd: Int = 4,
              @Query("speed") speed: Int = 1,
              @Query("title") title: String = "simi") : Observable<ResponseBody>

    @GET("/chebi/api/lyric_detail.php")
    fun lyricDetail(@Query("track_id") trackId: Long) : Observable<ResponseBody>
}