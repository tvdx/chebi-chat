package com.tinhvan.chebichat.core.bot

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.os.Parcel
import android.os.Parcelable
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.support.v4.content.ContextCompat
import android.text.Html
import android.util.Log
import com.baidu.tts.client.SpeechError
import com.baidu.tts.client.SpeechSynthesizer
import com.baidu.tts.client.SpeechSynthesizerListener
import com.baidu.tts.client.TtsMode
import com.google.android.gms.location.LocationServices
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.mklimek.sslutilsandroid.SslUtils
import com.tinhvan.chebichat.*
import com.tinhvan.chebichat.botchat.*
import com.tinhvan.chebichat.core.maps.GooglePlace
import com.tinhvan.chebichat.core.maps.PlaceItem
import com.tinhvan.chebichat.core.texttospeech.OfflineResource
import com.tinhvan.chebichat.settings.LocalSettings
import com.tinhvan.chebichat.settings.SettingsDataSource
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import io.reactivex.subjects.Subject
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.*

class ChebiBot constructor(val context: Context) {
    private val sslContext = SslUtils.getSslContextForCertificateFile(context, "chebi-chat.cer")
    private val gson = GsonBuilder().create()
    private val gsonConverter = GsonConverterFactory.create(gson)
    private val okHttpClient = OkHttpClient.Builder()
//            .sslSocketFactory(sslContext.socketFactory)
//            .hostnameVerifier { _, _ -> true }
//            .addNetworkInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    private val retrofit = Retrofit.Builder()
            .baseUrl("http://www.chebichat.com/")
//            .baseUrl("https://chebichat.co.192.168.51.93.xip.io/")
            .client(okHttpClient)
            .addConverterFactory(gsonConverter)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    private val apiService = retrofit.create(ChebiApiService::class.java)
    private val languageMap: Map<String, String> by lazy {
        hashMapOf("vn" to "vie-VNM",
                "zh" to "yue-CHN",
                "en" to "eng-USA")
    }
    private val settings: SettingsDataSource by lazy {
        LocalSettings(context)
    }
    private val auth: FirebaseAuth = FirebaseAuth.getInstance()
    private val offlineResources: List<OfflineResource> by lazy {
        val male = OfflineResource(context, "M")
        val female = OfflineResource(context, "F")
        val maleX = OfflineResource(context, "X")
        val femaleX = OfflineResource(context, "Y")
        listOf(female, male, male, maleX, femaleX)
    }
    private var mediaPlayer: MediaPlayer? = null
    private val googlePlace = GooglePlace(context)
    private val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
    private var googleTTSInit = false
    private lateinit var googleTextToSpeech: TextToSpeech
    val images = ArrayList<ImageInfo>()
    val strokes = HashMap<String, List<ImageInfo>>()

    private val speechSynthesizer: SpeechSynthesizer by lazy {
        SpeechSynthesizer.getInstance().apply {
            val app = context.packageManager.getApplicationInfo(context.packageName, PackageManager.GET_META_DATA)
            val bundle = app.metaData
            setContext(context)

            setAppId("${bundle.getInt("com.baidu.speech.APP_ID")}")
            setApiKey(bundle.getString("com.baidu.speech.API_KEY"), bundle.getString("com.baidu.speech.SECRET_KEY"))
            auth(TtsMode.MIX)

            setParam(SpeechSynthesizer.PARAM_SPEAKER, "0")
            setParam(SpeechSynthesizer.PARAM_MIX_MODE, SpeechSynthesizer.MIX_MODE_HIGH_SPEED_SYNTHESIZE_WIFI)
            setParam(SpeechSynthesizer.PARAM_TTS_TEXT_MODEL_FILE, offlineResources[1].textFilename)
            setParam(SpeechSynthesizer.PARAM_TTS_SPEECH_MODEL_FILE, offlineResources[1].modelFilename)

            initTts(TtsMode.MIX)
        }
    }

    fun sendChatMessage(message: String, nextPage: String? = null): Observable<ChatMessage> {
        val lang: String
        var pinyin: String? = null
        var displayText: String? = null
        when {
            Utils.isCJK(message) -> {
                lang = "zh"
                pinyin = Utils.toPinyin(message)
                displayText = generateDisplayText(message)
            }
            settings.getNuanceLanguage().take(1).blockingFirst() == "eng-USA" -> lang = "en"
            else -> lang = "vn"
        }
        val showPinyin = settings.getPinyin().take(1).blockingFirst()
        val adMessageSubject = ReplaySubject.create<ChatMessage>()!!
        val echo =
                Observable
                        .just(TextChatMessage(true,
                                message,
                                lang = lang,
                                pinyin = pinyin,
                                avatar = auth.currentUser?.photoUrl,
                                showPinyin = showPinyin,
                                displayText = displayText), TypingMessage())
                        .filter { nextPage == null }
        val chatObservable = apiService
                .sendMessage(lang, reqText = message, nextPage = nextPage, pageId = nextPage)
                .flatMap { it ->
                    if (it.msg == "OK") {
                        it.ad?.let {
                            loadAd(it.keyword, adMessageSubject)
                        }
                        if (it.property != null) {
                            when (it.property!!.type) {
                                "youtube" -> {
                                    return@flatMap Observable.just(
                                            ListVideoChatMessage(it.property!!.list!!.map { videoItem -> gson.fromJson(gson.toJsonTree(videoItem), VideoItem::class.java) },
                                                    input = message,
                                                    nextPage = it.property!!.nextPage)
                                    )
                                }
                                "picture" -> {
                                    images.add(ImageInfo(it.response, it.pinyin, it.property!!.image!!))

                                    return@flatMap Observable.just(
                                            getTextChatMessage(it, lang, showPinyin),
                                            ImageChatMessage(it.property!!.image!!))
                                }
                                "stroke" -> {
                                    val imageInfos = it.property!!.list!!.mapIndexed { index: Int, item: Any ->
                                        item as String
                                        ImageInfo(it.response!!.split(" ").last()[index].toString(), "stroke", item)
                                    }
                                    for (imageInfo in imageInfos) {
                                        strokes[imageInfo.url] = imageInfos
                                    }
                                    return@flatMap Observable.just(
                                            StrokeChatMessage(it.response!!, it.property!!.list!! as List<String>)
                                    )
                                }
                                "list" -> {
                                    val newsList = it.property!!.list!!.map { newsItem ->
                                        val result = gson.fromJson(gson.toJsonTree(newsItem), Item::class.java)
                                        if (it.property!!.code != 10003) {
                                            images.add(ImageInfo(result.name, result.info, result.icon!!))
                                        }
                                        result
                                    }
                                    return@flatMap Observable.just(ListGeneralChatMessage(message, newsList))
                                }
                                "photos" -> {
                                    val items = it.property!!.list!!.map { item ->
                                        val result = gson.fromJson(gson.toJsonTree(item), ImageItem::class.java)
                                        images.add(ImageInfo(result.name, result.info, result.icon!!))
                                        result
                                    }
                                    val nextPageId = it.property!!.nextPage?.run {
                                        split("&").singleOrNull { it.contains("pageid") }?.split("=")!![1]
                                    }
                                    return@flatMap Observable.just(ListGeneralChatMessage(message, items, nextPageId))
                                }
                                "url" -> {
                                    return@flatMap Observable.just(UrlChatMessage(it.property!!.text!!, it.property!!.url!!))
                                }
                                "wiki" -> {
                                    return@flatMap it.property!!.list!!
                                            .filter { it is List<*> }
                                            .map { Observable.fromIterable(it as List<String>) }
                                            .run {
                                                Observable.zip(this[0], this[1], this[2], Function3<String, String, String, WikiItem> { t1, t2, t3 ->
                                                    WikiItem(t1, t2, t3, lang = it.property!!.lang
                                                            ?: "vn")
                                                })
                                            }
                                            .toList()
                                            .flatMapObservable {
                                                Observable.just(ListChatMessage(WikiItem::class.java, message, it, null))
                                            }
                                }
                                "lyric" -> {
                                    return@flatMap Observable.fromIterable(it.property!!.list!!.withIndex())
                                            .concatMap {
                                                val lyricItem = it.value
                                                val item = gson.fromJson(gson.toJsonTree(lyricItem), LyricItem::class.java)
                                                if (it.index == 0) {
                                                    getLyricDetails(item.trackId)
                                                            .map {
                                                                item.copy(lyric = it.first, pinyinDisplay = it.second)
                                                            }
                                                } else {
                                                    Observable.just(
                                                            if (item.lyric != null && Utils.isCJK(item.lyric)) {
                                                                item.copy(pinyinDisplay = generateDisplayText(item.lyric))
                                                            } else {
                                                                item
                                                            })
                                                }
                                            }
                                            .toList()
                                            .flatMapObservable {
                                                Observable.just(ListLyricChatMessage(it, message, showPinyin))
                                            }
//                                    val listLyric = it.property!!.list!!.mapIndexed { index, lyricItem ->
//                                        val item = gson.fromJson(gson.toJsonTree(lyricItem), LyricItem::class.java)
//                                        if (index == 0 && item.lyric.isNullOrBlank()) {
//                                            val lyricDetails = getLyricDetails(item.trackId)
//                                                    .take(2, TimeUnit.SECONDS)
//                                                    .blockingFirst()
//
//                                            item.copy(lyric = lyricDetails.first, pinyinDisplay = lyricDetails.second)
//                                        } else {
//                                            if (item.lyric != null && Utils.isCJK(item.lyric)) {
//                                                item.copy(pinyinDisplay = generateDisplayText(item.lyric))
//                                            } else {
//                                                item
//                                            }
//                                        }
//                                    }
//                                    return@flatMap Observable.just(ListLyricChatMessage(listLyric, message, showPinyin))
                                }
                                "map" -> {
                                    return@flatMap getLocation().flatMap { location ->
                                        googlePlace.search(location.first, location.second, it.property!!.keyword!!)
                                                .filter { it.isNotEmpty() }
                                                .subscribeOn(Schedulers.io())
                                    }.map { ListChatMessage(PlaceItem::class.java, message, it, null) }
                                }
                            }
                        }

                        Observable.just(
                                getTextChatMessage(it, lang, showPinyin)
                        )
                    } else {
                        Observable.just(TextChatMessage(false, it.msg!!, lang = if (Utils.isCJK(it.msg!!)) "zh" else lang))
                    }
                }
        return Observable.merge(echo,
                chatObservable,
                adMessageSubject)
                .doOnError { Log.e("NguyenNK", "Error: ${it.message}", it) }
                .onErrorReturnItem(TextChatMessage(false, "No Response"))
    }

    fun translate(chatMessage: TextChatMessage): Observable<TranslatedTextChatMessage> {
        val translateMap: Map<String, String> by lazy {
            hashMapOf("vn" to "zh", "en" to "zh")
        }
        return settings.getNuanceLanguage()
                .take(1)
                .map {
                    if (chatMessage.lang in translateMap) {
                        translateMap[chatMessage.lang]!!
                    } else {
                        when (it) {
                            "eng-USA" -> "en_US"
                            else -> "vn"
                        }
                    }
                }
                .flatMap({ target ->
                    apiService
                            .translate(content = chatMessage.textMessage,
                                    lang = chatMessage.lang,
                                    target = target)
                }, { target: String, t2: JsonObject ->
                    val pinyin: String?
                    val displayText: String?
                    if (target == "zh") {
                        pinyin = Utils.toPinyin(t2["result"].asString)
                        displayText = generateDisplayText(t2["result"].asString)
                    } else {
                        pinyin = null
                        displayText = null
                    }
                    TranslatedTextChatMessage(mine = chatMessage.mine,
                            textMessage = t2["result"].asString,
                            showingActions = chatMessage.showingActions,
                            pinyin = pinyin,
                            lang = target,
                            displayText = displayText)
                })
    }

    private fun getTextChatMessage(it: ChebiResponse, lang: String, showPinyin: Boolean): ChatMessage {
        val language = if (Utils.isCJK(it.response!!)) "zh" else lang
        val pinyin: String?
        val displayText: String?
        if (language == "zh") {
            pinyin = Utils.toPinyin(it.response!!)
            displayText = generateDisplayText(it.response!!)
        } else {
            pinyin = it.pinyin
            displayText = null
        }
        return TextChatMessage(false,
                it.response!!,
                pinyin = pinyin,
                lang = language,
                showPinyin = showPinyin,
                displayText = displayText)
    }

    private fun generateDisplayText(message: String): String {
        return message.replace("<br>", "\n").replace("<br/>", "\n")
                .replace("</br>", "\n")
                .replace("|", "\n")
                .map {
                    if (it == '\n') {
                        it.toString()
                    } else {
                        val pinyinStr = Utils.toPinyin(it)
                        val spaces = " ".repeat(pinyinStr.length / 2)
                        "$spaces<ruby>$it<rt>$pinyinStr</rt></ruby>$spaces"
                    }
                }.reduce { acc, c ->
                    "$acc $c"
                }
    }

    fun speak(message: String, language: String): Observable<String> {
        val cjk = Utils.strip(message, true)
        val nonCjk = Utils.strip(message, false)

        Log.d("NguyenNK", "CJK = $cjk, length = ${cjk.length}")
        Log.d("NguyenNK", "Non CJK = $nonCjk, length = ${nonCjk.length}")

        val newMes: String
        val lang: String
        if (cjk.length > nonCjk.split(" ").size) {
            lang = "zh"
            newMes = Html.fromHtml(message).toString()
        } else {
            lang = language
            newMes = Html.fromHtml(message).toString()
        }

        return settings.getVoiceSpd(languageMap[lang]!!)
                .take(1)
                .zipWith(settings.getVoicePref(languageMap[lang]!!).take(1), BiFunction { voiceSpd: Int, voicePref: Int -> Pair(voiceSpd, voicePref) })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .flatMap { zipped: Pair<Int, Int> ->
                    val voiceSpd = zipped.first
                    val voicePref = zipped.second
                    val callback = PublishSubject.create<String>()

                    if (googleTTSInit) {
                        if (googleTextToSpeech.isSpeaking)
                            googleTextToSpeech.stop()
                    }
                    speechSynthesizer.stop()
                    mediaPlayer?.let {
                        if (it.isPlaying) it.stop()
                        it.release()
                        mediaPlayer = null
                    }
                    when (lang) {
                        "zh" -> {
                            speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEAKER, "$voicePref")
                            speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEED, "$voiceSpd")
                            speechSynthesizer.setSpeechSynthesizerListener(object : SpeechSynthesizerListener {
                                override fun onSynthesizeStart(p0: String?) {
                                }

                                override fun onSpeechFinish(p0: String?) {
                                    callback.onNext("Baidu speech finish")
                                    callback.onComplete()
                                }

                                override fun onSpeechProgressChanged(p0: String?, p1: Int) {
                                }

                                override fun onSynthesizeFinish(p0: String?) {
                                }

                                override fun onSpeechStart(p0: String?) {
                                }

                                override fun onSynthesizeDataArrived(p0: String?, p1: ByteArray?, p2: Int) {
                                }

                                override fun onError(p0: String?, p1: SpeechError?) {
                                    callback.onError(Exception("${p1?.code} - ${p1?.description}"))
                                }

                            })

                            var result = speechSynthesizer.loadModel(offlineResources[voicePref].modelFilename, offlineResources[voicePref].textFilename)
                            Log.d("NguyenNK", "Load model: $result")

                            result = speechSynthesizer.speak(newMes)
                            Log.d("NguyenNK", "Speak recognizedText: $result")
                        }
                        "en", "vn" -> {
                            val tmp = if (lang == "en") "$lang-US" else "vi-VN"
                            val listener = object : UtteranceProgressListener() {
                                override fun onDone(p0: String?) {
                                    callback.onNext("Google speech finished")
                                    callback.onComplete()
                                }

                                override fun onError(p0: String?) {
                                    callback.onError(Exception(p0))
                                }

                                override fun onStart(p0: String?) {
                                }
                            }
                            val params = hashMapOf(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID to newMes)
                            if (googleTTSInit) {
                                val result = googleTextToSpeech.setOnUtteranceProgressListener(listener)
                                googleTextToSpeech.language = Locale(tmp)
                                googleTextToSpeech.setSpeechRate(voiceSpd.toFloat() / 5.0f)
                                googleTextToSpeech.speak(newMes, TextToSpeech.QUEUE_FLUSH, params)
                            } else {
                                googleTextToSpeech = TextToSpeech(context) {
                                    if (it == 0) {
                                        googleTTSInit = true

                                        val result = googleTextToSpeech.setOnUtteranceProgressListener(listener)
                                        Log.d("NguyenNK", "Register listener = $result")
                                        googleTextToSpeech.language = Locale(tmp)
                                        googleTextToSpeech.setSpeechRate(voiceSpd.toFloat() / 5.0f)
                                        googleTextToSpeech.speak(newMes, TextToSpeech.QUEUE_FLUSH, params)
                                    }
                                }
                            }
                        }
                        else -> {
                            val voicePrefArray = arrayOf(
                                    "hn_male_xuantin_vdts_48k-hsmm",
                                    "hn_female_xuanthu_news_48k-hsmm",
                                    "hn_female_thutrang_phrase_48k-hsmm",
                                    "sg_male_xuankien_vdts_48k-hsmm",
                                    "sg_female_xuanhong_vdts_48k-hsmm"
                            )
                            val spd = (voiceSpd.toFloat() + 1) / 10f
                            val url = "https://tts.vbeecore.com/api/tts" +
                                    "?app_id=5b92574958a4933dcb329853&key=8eda558bcadf03058ee291d506222b5b" +
                                    "&voice=${voicePrefArray[voicePref]}" +
                                    "&rate=$spd" +
                                    "&time=1536327973912" +
                                    "&user_id=46280" +
                                    "&service_type=1" +
                                    "&input_text=$newMes"
                            Log.d("NguyenNK", "Sound url: $url")
                            playSound(url)
                        }
                    }
                    callback
                }
    }

    private fun playSound(source: String): Observable<String> {
        return Observable.create<String> { emitter ->
            mediaPlayer?.let {
                if (it.isPlaying) it.stop()
                it.release()
                mediaPlayer = null
            }
            mediaPlayer = MediaPlayer().apply {
                this.setOnPreparedListener {
                    it.start()
                    emitter.onNext("play from media")
                    emitter.onComplete()
                }
                this.setDataSource(source)
                this.prepareAsync()
            }
        }
    }

    private fun loadAd(keyword: String?, subject: Subject<ChatMessage>) {
//        AdLoader.Builder(context, "ca-app-pub-5139530126770446/8073807002") // PROD KEY
////        AdLoader.Builder(context, "ca-app-pub-3940256099942544/2247696110") // TEST KEY
//                .forUnifiedNativeAd {
//                    subject.onStop(AdChatMessage(it))
//                    subject.onComplete()
//                }
//                .withAdListener(object : AdListener() {
//                    override fun onAdFailedToLoad(p0: Int) {
//                        super.onAdFailedToLoad(p0)
//                        Log.d("NguyenNK", "Ad failed to load code: $p0")
//                        subject.onComplete()
//                    }
//                })
//                .withNativeAdOptions(NativeAdOptions.Builder().setImageOrientation(NativeAdOptions.ORIENTATION_LANDSCAPE).build())
//                .build()
//                .loadAd(AdRequest.Builder().addKeyword(keyword)
////                        .addTestDevice("413BE6FA7E57FC66806D4840FA006405")
//                        .build())
//        val nativeAd = NativeAd(context, "YOUR_PLACEMENT_ID")
        subject.onNext(AdChatMessage())
        subject.onComplete()
    }

    private fun getLocation(): Observable<Pair<Double, Double>> {
        return if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || fusedLocationProviderClient.lastLocation == null) {
            Observable.just(Pair(21.006471, 105.795355))
        } else {
            val result = PublishSubject.create<Pair<Double, Double>>()!!

            fusedLocationProviderClient.lastLocation.addOnCompleteListener {
                if (it.result != null) {
                    result.onNext(Pair(it.result.latitude, it.result.longitude))
                } else {
                    result.onNext(Pair(21.006471, 105.795355))
                }
                result.onComplete()
            }
            result
        }
    }

    fun getLyricDetails(trackId: Long): Observable<Pair<String, String?>> {
        return apiService.lyricDetail(trackId).map {
            val result = it.string()
            if (Utils.isCJK(result)) {
                Pair(result, generateDisplayText(result))
            } else {
                Pair(result, null)
            }
        }
    }

    fun shutdown() {
        if (googleTTSInit) {
            googleTextToSpeech.shutdown()
        }
        speechSynthesizer.stop()
    }

    data class ImageInfo(val name: String?, val description: String?, val url: String) : Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readString(),
                parcel.readString(),
                parcel.readString())

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(name)
            parcel.writeString(description)
            parcel.writeString(url)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<ImageInfo> {
            override fun createFromParcel(parcel: Parcel): ImageInfo {
                return ImageInfo(parcel)
            }

            override fun newArray(size: Int): Array<ImageInfo?> {
                return arrayOfNulls(size)
            }
        }
    }
}
