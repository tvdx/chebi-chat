package com.tinhvan.chebichat.core.bot

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ChebiResponse {

    @SerializedName("response")
    @Expose
    var response: String? = null
    @SerializedName("pinyin")
    @Expose
    var pinyin: String? = null
    @SerializedName("msg")
    @Expose
    var msg: String? = null
    @SerializedName("source")
    @Expose
    var source: String? = null
    @SerializedName("property")
    @Expose
    var property: Property? = null
    @SerializedName("language")
    @Expose
    var language: String? = null
    @SerializedName("ad")
    @Expose
    var ad: Ad? = null

    class Property {

        @SerializedName("type")
        @Expose
        var type: String? = null
        @SerializedName("list")
        @Expose
        var list: List<Any>? = null
        @SerializedName("nextpage", alternate = ["nextPage"])
        @Expose
        var nextPage: String? = null
        @SerializedName("url")
        @Expose
        var url: String? = null
        @SerializedName("image")
        @Expose
        var image: String? = null
        @SerializedName("text")
        @Expose
        var text: String? = null
        @SerializedName("code")
        @Expose
        var code: Int = 0
        @SerializedName("keyword")
        @Expose
        var keyword: String? = null
        @SerializedName("lang")
        @Expose
        var lang: String? = null
    }

    class Ad {
        @SerializedName("keyword")
        @Expose
        var keyword: String? = null

        @SerializedName("type")
        @Expose
        var type: String? = null
    }
}
