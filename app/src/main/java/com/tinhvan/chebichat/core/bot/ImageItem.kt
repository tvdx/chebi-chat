package com.tinhvan.chebichat.core.bot

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImageItem(
        @SerializedName("thumbnail")
        @Expose
        override val detailUrl: String?,
        @SerializedName("thumb_max")
        @Expose
        override val icon: String?,
        @SerializedName("description")
        @Expose
        override val info: String?,
        @SerializedName("title")
        @Expose
        override val name: String?
) : Item(detailUrl, icon, info, name)