package com.tinhvan.chebichat.core.bot

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class Item(
        @SerializedName("detailurl")
        @Expose
        open val detailUrl: String?,
        @SerializedName("icon")
        @Expose
        open val icon: String?,
        @SerializedName("info")
        @Expose
        open val info: String?,
        @SerializedName("name")
        @Expose
        open val name: String?
)
