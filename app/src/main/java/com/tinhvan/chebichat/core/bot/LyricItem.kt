package com.tinhvan.chebichat.core.bot

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LyricItem(
        @SerializedName("album_name")
        @Expose
        val albumName: String,
        @SerializedName("artist_name")
        @Expose
        val artistName: String,
        @SerializedName("lyric")
        @Expose
        val lyric: String? = null,
        @SerializedName("track_id")
        @Expose
        val trackId: Long,
        @SerializedName("track_name")
        @Expose
        val trackName: String,
        val loading: Boolean = false,
        val pinyinDisplay: String? = null,
        val showPinyin: Boolean = false
)