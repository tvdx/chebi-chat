package com.tinhvan.chebichat.core.bot

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VideoItem {
    @SerializedName("likeCount")
    @Expose
    var likeCount: String? = null
    @SerializedName("dislikeCount")
    @Expose
    var dislikeCount: String? = null
    @SerializedName("viewCount")
    @Expose
    var viewCount: String? = null
    @SerializedName("duration")
    @Expose
    var duration: String? = null
    @SerializedName("quality")
    @Expose
    var quality: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String? = null
}
