package com.tinhvan.chebichat.core.bot

data class WikiItem(val topic: String,
                    val description: String,
                    val detailUrl: String,
                    val translatedDesc: String? = null,
                    val lang: String = "vn",
                    val loadingTranslate: Boolean = false,
                    val loadingSpeak: Boolean = false)