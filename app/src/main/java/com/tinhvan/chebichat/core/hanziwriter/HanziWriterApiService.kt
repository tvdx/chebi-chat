package com.tinhvan.chebichat.core.hanziwriter

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.*

interface HanziWriterApiService {
    @POST("fanning")
    fun getFanning(@Body requestParams: RequestParams): Observable<List<String>>

    data class RequestParams(@SerializedName("input")
                             @Expose
                             val input: String,
                             @SerializedName("width")
                             @Expose
                             val width: Int = 1024,
                             @SerializedName("height")
                             @Expose
                             val height: Int = 1024)
}