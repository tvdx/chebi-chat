package com.tinhvan.chebichat.core.maps

import android.Manifest
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.content.PermissionChecker
import com.google.android.gms.location.LocationServices
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Math.*

class GooglePlace(private val context: Context) : PlaceDataSource {
    private val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://maps.googleapis.com")
            .build()
    private val apiService = retrofit.create(GooglePlaceApiService::class.java)
    private var currentLocation: Pair<Double, Double> = Pair(21.006471, 105.795355)
    private val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)

    override fun search(latitude: Double, longitude: Double, keyword: String): Observable<List<PlaceItem>> {
        val locationStr = "$latitude,$longitude"
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PermissionChecker.PERMISSION_GRANTED) {
            fusedLocationProviderClient.lastLocation.addOnCompleteListener {
                currentLocation = if (it.result != null) {
                    Pair(it.result.latitude, it.result.longitude)
                } else {
                    Pair(21.006471, 105.795355)
                }
            }
        }
        return apiService.nearBySearch(locationStr, keyword).map {
            it.results.map { result ->
                val photoUrl = if (result.photos.isNotEmpty()) {
                    "https://maps.googleapis.com/maps/api/place/photo" +
                            "?maxwidth=256" +
                            "&photoreference=${result.photos[0].photoReference}" +
                            "&key=AIzaSyBomHlfrJOmP412RlodYI1UNBs27cP20_E"
                } else {
                    result.icon
                }
                val placeLoc = result.geometry.location
                val distance = calculateDistance(placeLoc.lat, placeLoc.lng, currentLocation.first, currentLocation.second)
                PlaceItem(result.placeId, result.name, photoUrl, result.vicinity, (distance * 1000).toInt(), result.geometry.location.lat, result.geometry.location.lng, result.rating)
            }
        }
    }

    /**
     * Calculate distance between 2 pair of lat, lon
     */
    private fun calculateDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val λ1 = toRadians(lat1)
        val λ2 = toRadians(lat2)
        val Δλ = toRadians(lat2 - lat1)
        val Δφ = toRadians(lon2 - lon1)
        return 2 * R * asin(sqrt(pow(sin(Δλ / 2), 2.0) + pow(sin(Δφ / 2), 2.0) * cos(λ1) * cos(λ2)))
    }

    companion object {
        const val R = 6372.8 // in kilometers
    }
}