package com.tinhvan.chebichat.core.maps

import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface GooglePlaceApiService {
    @GET("/maps/api/place/nearbysearch/json?key=AIzaSyBomHlfrJOmP412RlodYI1UNBs27cP20_E&rankby=distance")
    fun nearBySearch(
            @Query("location") location: String,
            @Query("keyword") keyword: String
    ): Observable<GooglePlacesResponse>

    data class GooglePlacesResponse(
            @SerializedName("html_attributions") val htmlAttributions: List<Any> = listOf(),
            @SerializedName("results") val results: List<Result> = listOf(),
            @SerializedName("status") val status: String = ""
    ) {

        data class Result(
                @SerializedName("geometry") val geometry: Geometry = Geometry(),
                @SerializedName("icon") val icon: String = "",
                @SerializedName("id") val id: String = "",
                @SerializedName("name") val name: String = "",
                @SerializedName("opening_hours") val openingHours: OpeningHours = OpeningHours(),
                @SerializedName("photos") val photos: List<Photo> = listOf(),
                @SerializedName("place_id") val placeId: String = "",
                @SerializedName("plus_code") val plusCode: PlusCode = PlusCode(),
                @SerializedName("rating") val rating: Double = 0.0,
                @SerializedName("reference") val reference: String = "",
                @SerializedName("scope") val scope: String = "",
                @SerializedName("types") val types: List<String> = listOf(),
                @SerializedName("vicinity") val vicinity: String = ""
        ) {

            data class OpeningHours(
                    @SerializedName("open_now") val openNow: Boolean = false
            )


            data class Photo(
                    @SerializedName("height") val height: Int = 0,
                    @SerializedName("html_attributions") val htmlAttributions: List<String> = listOf(),
                    @SerializedName("photo_reference") val photoReference: String = "",
                    @SerializedName("width") val width: Int = 0
            )


            data class Geometry(
                    @SerializedName("location") val location: Location = Location(),
                    @SerializedName("viewport") val viewport: Viewport = Viewport()
            ) {

                data class Viewport(
                        @SerializedName("northeast") val northeast: Northeast = Northeast(),
                        @SerializedName("southwest") val southwest: Southwest = Southwest()
                ) {

                    data class Northeast(
                            @SerializedName("lat") val lat: Double = 0.0,
                            @SerializedName("lng") val lng: Double = 0.0
                    )


                    data class Southwest(
                            @SerializedName("lat") val lat: Double = 0.0,
                            @SerializedName("lng") val lng: Double = 0.0
                    )
                }


                data class Location(
                        @SerializedName("lat") val lat: Double = 0.0,
                        @SerializedName("lng") val lng: Double = 0.0
                )
            }


            data class PlusCode(
                    @SerializedName("compound_code") val compoundCode: String = "",
                    @SerializedName("global_code") val globalCode: String = ""
            )
        }
    }
}