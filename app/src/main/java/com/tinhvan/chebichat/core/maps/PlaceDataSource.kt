package com.tinhvan.chebichat.core.maps

import io.reactivex.Observable

interface PlaceDataSource {
    fun search(latitude: Double, longitude: Double, keyword: String): Observable<List<PlaceItem>>
}