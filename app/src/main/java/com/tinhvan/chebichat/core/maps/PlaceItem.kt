package com.tinhvan.chebichat.core.maps

import android.os.Parcel
import android.os.Parcelable

data class PlaceItem(val id: String,
                     val name: String,
                     val thumbnail: String,
                     val address: String,
                     val distance: Int,
                     val lat: Double,
                     val lng: Double,
                     val rating: Double = 0.0) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readDouble()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(thumbnail)
        parcel.writeString(address)
        parcel.writeInt(distance)
        parcel.writeDouble(lat)
        parcel.writeDouble(lng)
        parcel.writeDouble(rating)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PlaceItem> {
        override fun createFromParcel(parcel: Parcel): PlaceItem {
            return PlaceItem(parcel)
        }

        override fun newArray(size: Int): Array<PlaceItem?> {
            return arrayOfNulls(size)
        }
    }
}