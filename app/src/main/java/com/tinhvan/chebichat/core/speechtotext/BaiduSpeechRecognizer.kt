package com.tinhvan.chebichat.core.speechtotext

import android.content.Context
import android.media.AudioFormat
import android.media.MediaRecorder
import android.util.Base64
import android.util.Log
import com.baidu.speech.EventListener
import com.baidu.speech.EventManagerFactory
import com.baidu.speech.asr.SpeechConstant
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import omrecorder.*
import java.io.File
import java.util.concurrent.TimeUnit

class BaiduSpeechRecognizer(val context: Context) : SpeechRecognizer {
    private val asr = EventManagerFactory.create(context, "asr")
    private val gson = Gson()
    private val onVolume = PublishSubject.create<Float>()!!
    private val auth: FirebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }
    private val chebiFileUploadApiService = ChebiFileUploadApiService.getInstance()
    private var recorder: Recorder? = null

    override fun startRecording(): Observable<SpeechRecognizer.Event> {
        return Observable.create<SpeechRecognizer.Event> { emitter ->
            recorder = OmRecorder.pcm(PullTransport.Default(mic(),
                    PullTransport.OnAudioChunkPulledListener { audioChunk ->
                        onVolume.onNext(audioChunk.maxAmplitude().toFloat() - 30)
                    }),
                    file())
            recorder?.let {
                it.startRecording()
                emitter.onNext(SpeechRecognizer.Event(javaClass.name))
                onVolume.onNext(0f)
            }
        }
    }

    override fun stopRecording(): Observable<SpeechRecognizer.Recognition> {
        return Observable.create<SpeechRecognizer.Recognition> { emitter ->
            if (recorder == null) {
                emitter.onComplete()
                return@create
            }
            recorder!!.stopRecording()
            recorder = null
            val asr = EventManagerFactory.create(context, "asr")
            asr.registerListener(object : EventListener {
                override fun onEvent(name: String?, params: String?, p2: ByteArray?, p3: Int, p4: Int) {
                    Log.d("NguyenNK", "Stop record $name - $params")
                    when (name) {
                        SpeechConstant.CALLBACK_EVENT_ASR_PARTIAL -> {
                            try {
                                val jsonObject = gson.fromJson<JsonObject>(params, JsonObject::class.java)
                                if (jsonObject["result_type"].asString == "final_result") {
                                    Log.d("NguyenNK", "$name final result")
                                    val recognition: SpeechRecognizer.Recognition = gson.fromJson(params, SpeechRecognizer.Recognition::class.java)
                                    upload(recognition.bestResult)
                                    emitter.onNext(recognition)
                                }
                            } catch (ex: Exception) {
                                emitter.onError(ex)
                            }
                        }
                        SpeechConstant.CALLBACK_EVENT_ASR_FINISH -> {
                            emitter.onComplete()
                            asr.unregisterListener(this)
                            asr.send(SpeechConstant.ASR_STOP, null, null, 0, 0)
                        }
                    }
                }

            })
            val params: Map<String, Any> = hashMapOf(
                    SpeechConstant.ACCEPT_AUDIO_DATA to true,
                    SpeechConstant.ACCEPT_AUDIO_VOLUME to true,
                    SpeechConstant.VAD to "dnn",
                    SpeechConstant.DECODER to 2,
                    SpeechConstant.VAD_ENDPOINT_TIMEOUT to 0,
                    SpeechConstant.ACCEPT_AUDIO_DATA to false,
                    SpeechConstant.IN_FILE to file().path)
            asr.send(SpeechConstant.ASR_START, gson.toJson(params), null, 0, 0)
        }
                .timeout(5, TimeUnit.SECONDS, Observable.just(SpeechRecognizer.Recognition(emptyList(), "")))
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun audioLevel(): Observable<Float> {
        return onVolume
    }

    override fun cancel(): Observable<SpeechRecognizer.Event> {
        if (recorder == null) {
            return Observable.empty()
        }
        return Observable.fromCallable {
            recorder?.stopRecording()
            recorder = null
            SpeechRecognizer.Event("Canceled")
        }
    }

    private fun upload(bestResult: String) {
        Observable
                .defer {
                    val outFile = file()
                    val fileUploadRequest = FileUploadRequest(Base64.encodeToString(outFile.readBytes(), Base64.NO_WRAP), bestResult, auth.uid
                            ?: "anonymous", "yue-CHN")
                    chebiFileUploadApiService.fileUpload(fileUploadRequest)
                }
                .subscribeOn(Schedulers.io())
                .subscribe({
                    Log.d("NguyenNK", "Upload status: ${String(it.bytes())}")
                }, {
                    Log.e("NguyenNK", "Error uploading file: ${it.message}", it)
                })

    }

    private fun mic(): PullableSource {
        return PullableSource.Default(
                AudioRecordConfig.Default(
                        MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT,
                        AudioFormat.CHANNEL_IN_MONO, 16000
                )
        )
    }

    private fun file(): File {
        val file = File(context.filesDir, "baidu_input.pcm")
        if (!file.exists() && !file.createNewFile()) {
            Log.e("NguyenNK", "Cannot create ${file.path}")
        }
        return file
    }
}