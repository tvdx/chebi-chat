package com.tinhvan.chebichat.core.speechtotext

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

interface ChebiFileUploadApiService {
    companion object {
        fun getInstance(): ChebiFileUploadApiService {
            return Retrofit.Builder()
                    .baseUrl("http://voice.chebichat.com/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(ChebiFileUploadApiService::class.java)
        }
    }

    @POST("/file/upload/voices")
    fun fileUpload(@Body fileUploadRequest: FileUploadRequest): Observable<ResponseBody>
}

data class FileUploadRequest(
        @SerializedName("content")
        @Expose
        val content: String,
        @SerializedName("recognizedText")
        @Expose
        val recognizedText: String,
        @SerializedName("uid")
        @Expose
        val id: String,
        @SerializedName("language")
        @Expose
        val language: String)