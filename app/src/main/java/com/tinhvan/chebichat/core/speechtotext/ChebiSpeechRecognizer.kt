package com.tinhvan.chebichat.core.speechtotext

import com.tinhvan.chebichat.settings.SettingsDataSource
import io.reactivex.Observable

/**
 * Wrapper around underlying Speech Recognize engines
 * Determine the engine by current language
 */
class ChebiSpeechRecognizer(private val baiduSpeechRecognizer: SpeechRecognizer,
                            private val nuanceSpeechRecognizer: SpeechRecognizer,
                            private val settings: SettingsDataSource) : SpeechRecognizer {
    override fun startRecording(): Observable<SpeechRecognizer.Event> {
        return settings.getNuanceLanguage().take(1)
                .flatMap {
                    when (it) {
                        "yue-CHN" -> baiduSpeechRecognizer.startRecording()
                        else -> nuanceSpeechRecognizer.startRecording()
                    }
                }
    }

    override fun stopRecording(): Observable<SpeechRecognizer.Recognition> {
        return Observable.merge(
                baiduSpeechRecognizer.stopRecording(),
                nuanceSpeechRecognizer.stopRecording())
    }

    override fun audioLevel(): Observable<Float> {
        return Observable.merge(baiduSpeechRecognizer.audioLevel(),
                nuanceSpeechRecognizer.audioLevel())
    }

    override fun cancel(): Observable<SpeechRecognizer.Event> {
        return Observable.merge(baiduSpeechRecognizer.cancel(),
                nuanceSpeechRecognizer.cancel())
    }

}