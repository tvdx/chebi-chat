package com.tinhvan.chebichat.core.speechtotext

import android.content.Context
import android.media.AudioFormat
import android.media.MediaRecorder
import android.os.Environment
import android.util.Base64
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.tinhvan.chebichat.settings.LocalSettings
import com.tinhvan.chebichat.settings.SettingsDataSource
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import omrecorder.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.*
import java.io.File


/**
 * Call Nuance HTTP Api instead of using SDK
 */
class NuanceApiSpeechRecognizer(private val context: Context) : SpeechRecognizer {
    private val onStartRecording = PublishSubject.create<SpeechRecognizer.Event>()
    private val onFinishedRecording = PublishSubject.create<SpeechRecognizer.Event>()
    private val onRecognized = PublishSubject.create<SpeechRecognizer.Recognition>()
    private val onAudioLevel = PublishSubject.create<Float>()
    private var recorder: Recorder? = null
    private val nuanceApiService = NuanceApiService.getInstance()
    private val chebiFileUploadApiService = ChebiFileUploadApiService.getInstance()
    private val auth = FirebaseAuth.getInstance()
    private val settings: SettingsDataSource = LocalSettings(context)

    override fun startRecording(): Observable<SpeechRecognizer.Event> {
        return Observable.create<SpeechRecognizer.Event> { emitter ->
            recorder = OmRecorder.pcm(PullTransport.Default(mic(),
                    PullTransport.OnAudioChunkPulledListener { audioChunk ->
                        onAudioLevel.onNext(audioChunk.maxAmplitude().toFloat() - 30)
                    }),
                    file())
            recorder?.let {
                it.startRecording()
                emitter.onNext(SpeechRecognizer.Event(javaClass.name))
                onAudioLevel.onNext(0f)
            }
        }
//
//        return onStartRecording
    }

    override fun stopRecording(): Observable<SpeechRecognizer.Recognition> {
        if (recorder == null) {
            return Observable.empty()
        }
        recorder!!.stopRecording()
        val bytes = file().readBytes()
        val data = RequestBody.create(MediaType.parse("Content-Type: audio/x-wav;codec=pcm;bit=16;rate=16000"), bytes)
        val response =
                settings.getNuanceLanguage().take(1)
                        .flatMap({
                            nuanceApiService.dictation(data, auth.uid ?: "anonymous", it)
                                    .map {
                                        val results = String(it.bytes()).trim().split("\n")
                                        SpeechRecognizer.Recognition(results, results[0])
                                    }
                        }, { t1: String, t2: SpeechRecognizer.Recognition ->
                            Pair(t1, t2)
                        })

//                    .subscribe(onRecognized)

        response
                .subscribeOn(Schedulers.io())
                .flatMap {
                    val language = it.first
                    val recognition = it.second
                    val base64Encoded = Base64.encodeToString(bytes, Base64.NO_WRAP)

                    val fileUploadRequest = FileUploadRequest(base64Encoded,
                            recognition.bestResult, auth.uid ?: "anonymous", language)
                    Log.d("NguyenNK", "FileUploadRequest: $fileUploadRequest")

                    chebiFileUploadApiService.fileUpload(fileUploadRequest)
                }
                .subscribe({
                    Log.d("NguyenNK", "Upload status: ${String(it.bytes())}")
                }, {
                    Log.e("NguyenNK", "Failed to upload ${it.message}", it)
                })
        recorder = null
        return response
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.second }
    }

    override fun audioLevel(): Observable<Float> {
        return onAudioLevel
    }

    override fun cancel(): Observable<SpeechRecognizer.Event> {
        if (recorder == null) {
            return Observable.empty()
        }
        return Observable.fromCallable {
            recorder?.stopRecording()
            recorder = null
            SpeechRecognizer.Event("Canceled")
        }
    }

    private fun mic(): PullableSource {
        return PullableSource.Default(
                AudioRecordConfig.Default(
                        MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT,
                        AudioFormat.CHANNEL_IN_MONO, 16000
                )
        )
    }

    private fun file(): File {
        val file = File(context.filesDir, "om_input.pcm")
        if (!file.exists() && !file.createNewFile()) {
            Log.e("NguyenNK", "Cannot create ${file.path}")
        }
        return file
    }
}

interface NuanceApiService {
    @POST("/NMDPAsrCmdServlet/dictation?&id=12345678")
    @Headers("Content-Type: audio/x-wav;codec=pcm;bit=16;rate=16000",
            "Accept: text/plain")
    fun dictation(@Body data: RequestBody,
                  @Query("id") id: String,
                  @Header("Accept-Language") lang: String,
                  @Query("appId") appId: String = "NMDPTRIAL_nguyennk92_gmail_com20180715214547",
                  @Query("appKey") appKey: String = "216c001435c44e2a7c8742254a7bf918650d07277b9b9f92bf194018bf5de7e0c9e7f4feac432928ca6b4d14e7462c4c533373cfcf13618e2ca1a6ecfe8dccc8"): Observable<ResponseBody>

    companion object {
        fun getInstance(): NuanceApiService {
            return Retrofit.Builder()
                    .baseUrl("https://dictation.nuancemobility.net/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
                    .create(NuanceApiService::class.java)
        }
    }
}

