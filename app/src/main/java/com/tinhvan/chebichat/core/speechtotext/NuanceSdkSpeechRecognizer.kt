package com.tinhvan.chebichat.core.speechtotext

import android.content.Context
import android.net.Uri
import android.util.Log
import com.nuance.speechkit.*
import com.tinhvan.chebichat.settings.LocalSettings
import com.tinhvan.chebichat.settings.SettingsDataSource
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class NuanceSdkSpeechRecognizer(private val context: Context) : SpeechRecognizer {
    private var transaction: Transaction? = null
    private val settings: SettingsDataSource = LocalSettings(context)

    val onStartedRecording = PublishSubject.create<SpeechRecognizer.Event>()!!
    val onFinishedRecording = PublishSubject.create<SpeechRecognizer.Event>()!!
    val onRecognition = PublishSubject.create<SpeechRecognizer.Recognition>()!!
    private lateinit var language: String

    init {
        settings.getNuanceLanguage().subscribe {
            language = it
        }
    }

    override fun startRecording(): Observable<SpeechRecognizer.Event> {
        val options = Transaction.Options().also {
            it.recognitionType = RecognitionType.DICTATION
            it.detection = DetectionType.None
            it.language = Language(language)
        }
        val session = Session.Factory.session(context, Uri.parse(NuanceConfig.SERVER_URI), NuanceConfig.APP_KEY)
        session.audioPlayer.setListener(object : AudioPlayer.Listener {
            override fun onBeginPlaying(p0: AudioPlayer?, p1: Audio?) {
                Log.d("NguyenNK", "Audio: $p1")
            }

            override fun onFinishedPlaying(p0: AudioPlayer?, p1: Audio?) {
                Log.d("NguyenNK", "Audio: $p1")
            }

        })

        transaction = session.recognize(options, object : Transaction.Listener() {
            override fun onStartedRecording(transaction: Transaction) {
                super.onStartedRecording(transaction)
                onStartedRecording.onNext(SpeechRecognizer.Event(javaClass.name))
            }

            override fun onFinishedRecording(transaction: Transaction) {
                super.onFinishedRecording(transaction)
                onFinishedRecording.onNext(SpeechRecognizer.Event(javaClass.name))
            }

            override fun onRecognition(transaction: Transaction, recognition: Recognition) {
                super.onRecognition(transaction, recognition)

                onRecognition.onNext(SpeechRecognizer.Recognition(recognition.details.map {
                    it.text
                }, recognition.text))
            }

            override fun onServiceResponse(p0: Transaction?, p1: JSONObject?) {
                super.onServiceResponse(p0, p1)
                Log.d("NguyenNK", "onServiceResponse: $p1, $p0")
            }

            override fun onAudio(p0: Transaction?, p1: Audio?) {
                super.onAudio(p0, p1)
                Log.d("NguyenNK", "Audio: $p1")
            }

            override fun onError(p0: Transaction?, p1: String?, p2: TransactionException?) {
                super.onError(p0, p1, p2)
                Log.e("NguyenNK", "Error: $p1")
            }

            override fun onInterpretation(p0: Transaction?, p1: Interpretation?) {
                super.onInterpretation(p0, p1)
                Log.e("NguyenNK", "onInterpretation: $p1")
            }

            override fun onSuccess(p0: Transaction?, p1: String?) {
                super.onSuccess(p0, p1)
                Log.e("NguyenNK", "onSuccess: $p1")
            }
        })

        return onStartedRecording.take(1)
    }

    override fun stopRecording(): Observable<SpeechRecognizer.Recognition> {
        transaction?.stopRecording()
        transaction = null
        return onRecognition
    }

    fun isRecording(): Boolean {
        return transaction != null
    }

    override fun audioLevel(): Observable<Float> {
        val observable = Observable
                .interval(50, TimeUnit.MILLISECONDS)
                .map { if (transaction != null) transaction!!.audioLevel else 0.0f }
        observable.publish().connect()
        return observable
    }

    override fun cancel(): Observable<SpeechRecognizer.Event> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
