package com.tinhvan.chebichat.core.speechtotext

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.POST

interface SpeechRecognizeApiService {
    @POST("/file/upload")
    fun uploadFile()

    fun getInstance(): SpeechRecognizeApiService {
        return Retrofit.Builder()
                .baseUrl("https://dictation.nuancemobility.net/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(javaClass)
    }
}