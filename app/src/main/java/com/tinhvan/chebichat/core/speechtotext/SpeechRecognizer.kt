package com.tinhvan.chebichat.core.speechtotext

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable

interface SpeechRecognizer {
    fun startRecording(): Observable<Event>
    fun stopRecording(): Observable<Recognition>
    fun audioLevel(): Observable<Float>
    fun cancel(): Observable<Event>

    data class Event(val name: String)

    data class Recognition(
            @SerializedName("results_recognition")
            @Expose
            var resultsRecognition: List<String>,
            @SerializedName("best_result")
            @Expose
            val bestResult: String)
}