package com.tinhvan.chebichat.core.wakeup

import android.content.Context
import android.content.pm.PackageManager
import android.media.AudioFormat
import android.media.MediaRecorder
import android.util.Log
import com.baidu.speech.EventListener
import com.baidu.speech.EventManager
import com.baidu.speech.EventManagerFactory
import com.baidu.speech.asr.SpeechConstant
import io.reactivex.subjects.PublishSubject
import omrecorder.AudioRecordConfig
import omrecorder.PullableSource
import org.json.JSONObject
import java.io.File

class BaiduWakeUp(val context: Context) : WakeUp {
    override val onError: PublishSubject<WakeUpResult> = PublishSubject.create()
    override val onStop: PublishSubject<String> = PublishSubject.create()
    override val onWakeUp: PublishSubject<WakeUpResult> = PublishSubject.create()
    private var result: WakeUpResult? = null
    private val listener = EventListener { name: String?, params: String?, data: ByteArray?, offset: Int, length: Int ->
        Log.d("NguyenNK", "Name: $name, params: $params")
        if (SpeechConstant.CALLBACK_EVENT_WAKEUP_SUCCESS == name) { // 识别唤醒词成功
            result = WakeUpResult.parseJson(name, params)
            if (result?.hasError() == false) { // error不为0依旧有可能是异常情况
                wp?.send(SpeechConstant.WAKEUP_STOP, null, null, 0, 0)
            } else {
                onError.onNext(result ?: WakeUpResult().apply { errorCode = -1 })
            }
        } else if (SpeechConstant.CALLBACK_EVENT_WAKEUP_ERROR == name) { // 识别唤醒词报错
            val result = WakeUpResult.parseJson(name, params)
            if (result.hasError()) {
                onError.onNext(result)
            }
        } else if (SpeechConstant.CALLBACK_EVENT_WAKEUP_STOPED == name) { // 关闭唤醒词
            if (result != null) {
                onWakeUp.onNext(result)
                result = null
            } else {
                onStop.onNext("Finish")
            }
        } else if (SpeechConstant.CALLBACK_EVENT_WAKEUP_AUDIO == name) { // 音频回调
        }
    }

    private var wp: EventManager? = EventManagerFactory.create(context, "wp").apply {
        registerListener(listener)
    }

    override fun start() {
        val params = HashMap<String, Any>()
        val app = context.packageManager.getApplicationInfo(context.packageName, PackageManager.GET_META_DATA)
        val bundle = app.metaData

        params[SpeechConstant.WP_WORDS_FILE] = "assets:///chebi_tongxue.bin"
        params[SpeechConstant.APP_ID] = "${bundle.getInt("com.baidu.speech.APP_ID")}"
        params[SpeechConstant.APP_KEY] = bundle.getString("com.baidu.speech.API_KEY")
        params[SpeechConstant.SECRET] = bundle.getString("com.baidu.speech.SECRET_KEY")

        // params.put(SpeechConstant.ACCEPT_AUDIO_DATA,true);
        // params.put(SpeechConstant.ACCEPT_AUDIO_VOLUME,true);
        // params.put(SpeechConstant.IN_FILE,"res:///com/baidu/android/voicedemo/wakeup.pcm");
        // params里 "assets:///WakeUp.bin" 表示WakeUp.bin文件定义在assets目录下
        wp?.send(SpeechConstant.WAKEUP_START, JSONObject(params).toString(), null, 0, 0)
    }

    override fun cancel() {
        wp?.let {
            it.send(SpeechConstant.WAKEUP_STOP, null, null, 0, 0)
//            it.unregisterListener(listener)
        }
    }
}