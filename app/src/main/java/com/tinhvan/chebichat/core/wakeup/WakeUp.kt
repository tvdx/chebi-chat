package com.tinhvan.chebichat.core.wakeup

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

interface WakeUp {
    val onStop: PublishSubject<String>
    val onWakeUp: PublishSubject<WakeUpResult>
    val onError: PublishSubject<WakeUpResult>

    fun start()
    fun cancel()
}