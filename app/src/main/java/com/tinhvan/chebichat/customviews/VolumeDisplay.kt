package com.tinhvan.chebichat.customviews

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.tinhvan.chebichat.R
import kotlinx.android.synthetic.main.volume_display.view.*

class VolumeDisplay : LinearLayout {
//    override val containerView: View?
//        get() = this

    private val volumeBars: List<View> by lazy {
        listOf(view1, view2, view3, view4, view5)
    }

    var minVolume = 30.0f
    var maxVolume = 70.0f

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        LayoutInflater.from(context).inflate(R.layout.volume_display, this)
    }

    fun setVolume(volume: Float) {
        val barUnit = (maxVolume - minVolume) / volumeBars.size
        val unit = ((volume - minVolume) / barUnit).toInt()
        for (i in volumeBars.indices) {
            val volumeBar = volumeBars[i]
            if (i < unit) {
                volumeBar.setBackgroundColor(Color.WHITE)
            } else {
                volumeBar.setBackgroundColor(Color.parseColor("#11ffffff"))
            }
        }
    }
}
