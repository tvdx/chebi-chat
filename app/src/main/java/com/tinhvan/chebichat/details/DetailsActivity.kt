package com.tinhvan.chebichat.details

import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_SEND
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import com.facebook.ads.*
import com.jakewharton.rxbinding2.view.clicks
import com.tinhvan.chebichat.R
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_details.*
import com.tinhvan.chebichat.R.id.adView
import com.facebook.ads.AdIconView
import android.widget.TextView
import android.widget.RelativeLayout
import android.widget.LinearLayout
import android.view.LayoutInflater
import com.facebook.ads.NativeBannerAd
import kotlinx.android.synthetic.main.native_banner_ad_unit.*


class DetailsActivity : AppCompatActivity() {
    companion object {
        fun newIntent(context: Context, url: String): Intent {
            return Intent(context, DetailsActivity::class.java).apply {
                putExtra("extra_url", url)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val url = intent.getStringExtra("extra_url")
        webView.apply {
            webChromeClient = object : WebChromeClient() {
                override fun onProgressChanged(view: WebView?, progress: Int) {
                    if (progress < 100 && progressBar.visibility == ProgressBar.GONE) {
                        progressBar.visibility = ProgressBar.VISIBLE
                    }

                    progressBar.progress = progress
                    if (progress == 100) {
                        progressBar.visibility = ProgressBar.GONE
                    }
                }
            }
            webViewClient = WebViewClient()
            settings.apply {
                javaScriptEnabled = true
                builtInZoomControls = true
                domStorageEnabled = true
                databaseEnabled = true
                setAppCacheEnabled(true)
                scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
            }
            loadUrl(url)
        }

        btnBack.clicks().subscribe {
            finish()
        }
        btnShare.clicks().subscribe {
            startActivity(Intent().apply {
                action = ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, "${url}\n\n( Từ https://www.chebichat.com)")
                type = "text/plain"
            })
        }

//        adView.adListener = object: AdListener() {
//            override fun onAdLoaded() {
//                super.onAdLoaded()
//                Log.d("NguyenNK", "Banner loaded")
//            }
//            override fun onAdFailedToLoad(p0: Int) {
//                super.onAdFailedToLoad(p0)
//                Log.d("NguyenNK", "Banner failed to load code $p0")
//            }
//        }
//        AdRequest.Builder()
//                .addTestDevice("413BE6FA7E57FC66806D4840FA006405")
//                .build().run {
//                    adView.loadAd(this)
//                }

        adView.visibility = View.GONE
        val nativeBannerAd = NativeBannerAd(this, getString(R.string.fb_native_banner_ad_id))
        nativeBannerAd.setAdListener(object: NativeAdListener{
            override fun onAdClicked(p0: Ad?) {
            }

            override fun onMediaDownloaded(p0: Ad?) {
            }

            override fun onError(p0: Ad?, p1: AdError?) {
                Log.d("NguyenNK", "Banner failed to load code ${p1?.errorMessage}")
            }

            override fun onAdLoaded(p0: Ad?) {
                Log.d("NguyenNK", "Banner ad loaded")
                inflateAd(nativeBannerAd)
            }

            override fun onLoggingImpression(p0: Ad?) {
            }

        })
        nativeBannerAd.loadAd()
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            super.onBackPressed()
        }
    }

    private fun inflateAd(nativeBannerAd: NativeBannerAd) {
        // Unregister last ad
        nativeBannerAd.unregisterView()
        adView.visibility = View.VISIBLE

        // Add the AdChoices icon
        val adChoicesContainer = ad_choices_container
        val adChoicesView = AdChoicesView(this, nativeBannerAd, true)
        adChoicesContainer.addView(adChoicesView, 0)

        // Create native UI using the ad metadata.
        val nativeAdTitle = native_ad_title
        val nativeAdSocialContext = native_ad_social_context
        val sponsoredLabel = native_ad_sponsored_label
        val nativeAdIconView = native_icon_view
        val nativeAdCallToAction = native_ad_call_to_action

        // Set the Text.
        nativeAdCallToAction.text = nativeBannerAd.adCallToAction
        nativeAdCallToAction.visibility = if (nativeBannerAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE
        nativeAdTitle.text = nativeBannerAd.advertiserName
        nativeAdSocialContext.text = nativeBannerAd.adSocialContext
        sponsoredLabel.text = nativeBannerAd.sponsoredTranslation

        // Register the Title and CTA button to listen for clicks.
        val clickableViews = ArrayList<View>()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)
        nativeBannerAd.registerViewForInteraction(adView, nativeAdIconView, clickableViews)
    }
}
