package com.tinhvan.chebichat.gallery

import android.graphics.drawable.PictureDrawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.caverock.androidsvg.SVG
import com.tinhvan.chebichat.GlideApp
import com.tinhvan.chebichat.GlideOptions
import com.tinhvan.chebichat.R
import com.tinhvan.chebichat.core.hanziwriter.HanziWriterApiService
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_image_view.*

class GalleryFanningStrokeAdapter(private val data: List<String>)
    : RecyclerView.Adapter<GalleryFanningStrokeAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_image_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val svg = SVG.getFromString(data[position])
        holder.image.setImageDrawable(PictureDrawable(svg.renderToPicture()))
//        GlideApp.with(holder.itemView)
//                .load(data[position])
//                .apply(GlideOptions.fitCenterTransform())
//                .into(holder.image)
    }

    class ViewHolder(override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer
}
