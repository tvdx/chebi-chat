package com.tinhvan.chebichat.gallery

import android.Manifest
import android.app.Dialog
import android.app.DownloadManager
import android.app.Service
import android.content.Intent
import android.content.Intent.ACTION_SEND
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RelativeLayout
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.ads.AdRequest
import com.jakewharton.rxbinding2.support.v4.view.RxViewPager
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.text
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.single.SnackbarOnDeniedPermissionListener
import com.tinhvan.chebichat.GlideApp
import com.tinhvan.chebichat.R
import com.tinhvan.chebichat.Utils
import com.tinhvan.chebichat.core.bot.ChebiBot
import kotlinx.android.synthetic.main.fragment_gallery.*
import java.io.File
import java.io.FileOutputStream


class GalleryFragment : DialogFragment() {
    private val EXTRA_URLS = "extra_urls"

    companion object {
        fun newInstance(imageInfo: List<ChebiBot.ImageInfo>, selected: Int): GalleryFragment {
            return GalleryFragment().apply {
                val args = Bundle()
                args.putParcelableArrayList(EXTRA_URLS, ArrayList(imageInfo))
                args.putInt("selected", selected)
                arguments = args
            }
        }
    }

//    override fun onStart() {
//        super.onStart()
//        dialog.apply {
//            val width = ViewGroup.LayoutParams.MATCH_PARENT;
//            val height = ViewGroup.LayoutParams.MATCH_PARENT;
//            window.setLayout(width, height)
//        }
//    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // the content
        val root = RelativeLayout(activity!!)
        root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        // creating the fullscreen dialog
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_gallery, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvTitle.isSelected = true
        arguments?.let {
            val imageInfo = it.getParcelableArrayList<ChebiBot.ImageInfo>(EXTRA_URLS)
            val selected = it.getInt("selected", 0)
            val pagerAdapter = GalleryPagerAdapter(childFragmentManager, imageInfo!!)

            viewPager.adapter = pagerAdapter
            RxViewPager.pageSelections(viewPager)
                    .map {
                        val name = imageInfo[it].name?:""
                        Html.fromHtml(name).toString() + " (${Utils.toPinyin(name)})"
                    }.subscribe(tvTitle.text())

            viewPager.setCurrentItem(selected, false)

            btnSave.clicks()
                    .map { imageInfo[viewPager.currentItem].url }
                    .subscribe { url ->
                        if (!storagePermissionGranted()) {
                            requestStoragePermission()
                        }

                        if (storagePermissionGranted()) {
                            Toast.makeText(activity, "Started downloading", Toast.LENGTH_SHORT).show()
                            val end = if (url.indexOf("?") != -1) url.indexOf("?") else url.length
                            var fileName = url.substring(url.lastIndexOf("/") + 1, end)
                            if (!fileName.contains(".")) {
                                if (fileName.contains("gif")) {
                                    fileName += ".gif"
                                }
                            }
                            val request = DownloadManager.Request(Uri.parse(url))
                            request.setTitle(fileName)
                            request.setDescription("Downloading from $url")
                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, "Chebi/$fileName")
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                            request.allowScanningByMediaScanner()

                            val downloadManager = activity?.getSystemService(Service.DOWNLOAD_SERVICE) as DownloadManager
                            downloadManager?.enqueue(request)
                        }
                    }

            btnShare.clicks()
                    .map { imageInfo[viewPager.currentItem].url }
                    .subscribe { url ->
                        GlideApp.with(this)
                                .asBitmap()
                                .load(url)
                                .into(object : SimpleTarget<Bitmap>() {
                                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                        val file = File(activity!!.externalCacheDir, "tmp.png")
                                        val fOut = FileOutputStream(file)
                                        resource.compress(Bitmap.CompressFormat.PNG, 100, fOut)
                                        fOut.flush()
                                        fOut.close()
                                        startActivity(Intent().apply {
                                            action = ACTION_SEND
                                            putExtra(Intent.EXTRA_SUBJECT, "Từ https://www.chebichat.com")
                                            putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(activity!!.applicationContext, "com.tinhvan.chebichat.provider", file))
                                            putExtra(Intent.EXTRA_TEXT, "Từ https://www.chebichat.com)")
                                            type = "*/*"
                                        })
                                    }
                                })
                    }
        }

        AdRequest.Builder()
//                .addTestDevice("413BE6FA7E57FC66806D4840FA006405")
                .build().also {
                    adView.loadAd(it)
                }
    }

    private fun storagePermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestStoragePermission() {
        val listener = SnackbarOnDeniedPermissionListener.Builder.with(view, "Storage permission is required to download")
                .build()
        Dexter.withActivity(activity)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(listener)
                .onSameThread()
                .check()
    }
}