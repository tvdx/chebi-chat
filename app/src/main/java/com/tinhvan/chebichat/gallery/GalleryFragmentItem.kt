package com.tinhvan.chebichat.gallery

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tinhvan.chebichat.GlideApp
import com.tinhvan.chebichat.R
import com.tinhvan.chebichat.core.bot.ChebiBot
import kotlinx.android.synthetic.main.item_gallery_image.view.*

class GalleryFragmentItem : Fragment() {
    companion object {
        fun newInstance(imageInfo: ChebiBot.ImageInfo): GalleryFragmentItem {
            val result = GalleryFragmentItem()
            val args = Bundle()
            args.putParcelable("image_info", imageInfo)

            result.arguments = args
            return result
        }
    }

    private lateinit var imageInfo: ChebiBot.ImageInfo

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.item_gallery_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            imageInfo = it.getParcelable("image_info")

            GlideApp.with(view)
                    .load(imageInfo.url)
                    .fitCenter()
                    .into(view.image)
        }
    }
}