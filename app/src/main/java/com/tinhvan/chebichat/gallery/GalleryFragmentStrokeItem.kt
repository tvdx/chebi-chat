package com.tinhvan.chebichat.gallery

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.request.ImageRequest
import com.jakewharton.rxbinding2.support.design.widget.selections
import com.jakewharton.rxbinding2.view.visibility
import com.tinhvan.chebichat.R
import com.tinhvan.chebichat.core.bot.ChebiBot
import com.tinhvan.chebichat.core.hanziwriter.HanziWriterApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_gallery_stroke_image.*
import kotlinx.android.synthetic.main.item_gallery_stroke_image.view.*
import org.koin.android.ext.android.inject
import kotlin.math.roundToInt

class GalleryFragmentStrokeItem : Fragment() {
    private val hanziWriterApiService: HanziWriterApiService by inject()

    companion object {
        fun newInstance(imageInfo: ChebiBot.ImageInfo): GalleryFragmentStrokeItem {
            val result = GalleryFragmentStrokeItem()
            val args = Bundle()
            args.putParcelable("image_info", imageInfo)

            result.arguments = args
            return result
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.item_gallery_stroke_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabLayout.addTab(tabLayout.newTab().setText("GIF"), true)
        tabLayout.addTab(tabLayout.newTab().setText("Fanning"))

        tabLayout.selections()
                .share().apply {
                    map { it.position == 0 }.subscribe(imageGif.visibility())
                    map { it.position == 1 }.subscribe(rvFanningStrokes.visibility())
                }

        arguments?.let {
            val imageInfo = it.getParcelable<ChebiBot.ImageInfo>("image_info")

            view.imageGif
                    .controller = Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequest.fromUri(imageInfo.url))
                    .setAutoPlayAnimations(true)
                    .build()
            hanziWriterApiService.getFanning(HanziWriterApiService.RequestParams(imageInfo.name!!, 240, 240))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        var spanCount = Math.sqrt(it.size.toDouble()).roundToInt().toDouble()
                        if (spanCount < it.size.toDouble() / spanCount) spanCount += 1.0
                        val layoutManger = GridLayoutManager(activity, spanCount.roundToInt(), GridLayoutManager.VERTICAL, false)
                        val adapter = GalleryFanningStrokeAdapter(it)
                        rvFanningStrokes.adapter = adapter
                        rvFanningStrokes.layoutManager = layoutManger
                    }, {
                        Log.e("NguyenNK", "Fanning error ${it.message}", it)
                    })
        }

    }
}
