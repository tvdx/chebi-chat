package com.tinhvan.chebichat.gallery

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.tinhvan.chebichat.core.bot.ChebiBot

class GalleryPagerAdapter(fragmentManager: FragmentManager,
                          private val images: List<ChebiBot.ImageInfo>) : FragmentStatePagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment {
        val imageInfo = images[position]
        return if (imageInfo.description == "stroke") {
            GalleryFragmentStrokeItem.newInstance(images[position])
        } else {
            GalleryFragmentItem.newInstance(images[position])
        }
    }

    override fun getCount(): Int {
        return images.size
    }
}