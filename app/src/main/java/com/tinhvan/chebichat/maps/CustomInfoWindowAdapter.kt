package com.tinhvan.chebichat.maps

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.ImageViewTarget
import com.bumptech.glide.request.target.Target
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.tinhvan.chebichat.GlideApp
import com.tinhvan.chebichat.R
import kotlinx.android.synthetic.main.map_marker_info.view.*

class CustomInfoWindowAdapter(private val context: Context) : GoogleMap.InfoWindowAdapter {
    private val loaded = HashSet<Any>()
    override fun getInfoContents(marker: Marker?): View? {
        val view = LayoutInflater.from(context)
                .inflate(R.layout.map_marker_info, null)
        marker?.let {
            GlideApp.with(view)
                    .load(it.tag)
                    .centerCrop()
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            if (!loaded.contains(it.id)) {
                                loaded.add(it.id)
                                it.showInfoWindow()
                            }
                            return false
                        }
                    })
                    .into(view.imvIcon)
        }
        view.tvName.text = marker?.title
        view.tvAddress.text = marker?.snippet
        return view
    }

    override fun getInfoWindow(p0: Marker?): View? {
        return null
    }
}