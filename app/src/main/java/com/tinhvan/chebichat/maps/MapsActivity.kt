package com.tinhvan.chebichat.maps

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.PermissionChecker
import android.view.View
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*

import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.jakewharton.rxbinding2.view.clicks
import com.tinhvan.chebichat.GlideApp
import com.tinhvan.chebichat.R
import com.tinhvan.chebichat.core.maps.PlaceItem
import kotlinx.android.synthetic.main.activity_maps.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap
    private lateinit var placeItems: List<PlaceItem>
    private val fusedLocationProviderClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(this)
    }

    companion object {
        fun newIntent(context: Context, placeItems: List<PlaceItem>): Intent {
            return Intent(context, MapsActivity::class.java).apply {
                putParcelableArrayListExtra("extra_place_items", if (placeItems is ArrayList<*>) placeItems as ArrayList<PlaceItem> else ArrayList(placeItems))
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        placeItems = intent.getParcelableArrayListExtra("extra_place_items")

        val mapFragment = map as SupportMapFragment
        mapFragment.getMapAsync(this)

        val locationGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PermissionChecker.PERMISSION_GRANTED
        if (placeItems.size == 1) {
            btnDirection.clicks().subscribe {
                if (locationGranted) {
                    fusedLocationProviderClient.lastLocation.addOnCompleteListener {
                        val result = it.result
                        val intent = Intent(android.content.Intent.ACTION_VIEW, Uri.parse(
                                "http://maps.google.com/maps?saddr=${result.latitude},${result.longitude}&daddr=${placeItems[0].lat},${placeItems[0].lng}"))
                        startActivity(intent)
                    }
                } else {
                    val intent = Intent(android.content.Intent.ACTION_VIEW, Uri.parse(
                            "http://maps.google.com/maps?saddr=21.006471, 105.795355&daddr=${placeItems[0].lat},${placeItems[0].lng}"))
                    startActivity(intent)
                }
            }
            btnMap.clicks().subscribe{
                val intent = Intent(android.content.Intent.ACTION_VIEW, Uri.parse(
                        "http://maps.google.com/maps?q=${placeItems[0].lat},${placeItems[0].lng}"))
                startActivity(intent)
            }
            btnDirection.visibility = View.VISIBLE
            btnMap.visibility = View.VISIBLE

        } else {
            btnDirection.visibility = View.GONE
            btnMap.visibility = View.GONE
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setInfoWindowAdapter(CustomInfoWindowAdapter(this))

        placeItems.forEach {
            val markerOptions = MarkerOptions().title(it.name)
                    .position(LatLng(it.lat, it.lng))
                    .snippet(it.address)
            mMap.addMarker(markerOptions).also { marker: Marker ->
                marker.tag = it.thumbnail
                if (placeItems.size == 1) marker.showInfoWindow()
            }
        }


        val locationGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PermissionChecker.PERMISSION_GRANTED
        mMap.isMyLocationEnabled = locationGranted

        if (locationGranted && placeItems.size > 1) {
            fusedLocationProviderClient.lastLocation.addOnCompleteListener {
                val result = it.result
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(result.latitude, result.longitude), 13f))
            }
        } else {
            placeItems[0].let {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(it.lat, it.lng), 16f))
                mMap.uiSettings.isMapToolbarEnabled = true
                mMap.setOnMarkerClickListener {
                    it.showInfoWindow()
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(it.position))
                    true
                }
            }
        }
    }
}
