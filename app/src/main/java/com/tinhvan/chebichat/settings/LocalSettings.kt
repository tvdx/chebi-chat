package com.tinhvan.chebichat.settings

import android.content.Context
import android.util.Log
import com.f2prateek.rx.preferences2.RxSharedPreferences
import io.reactivex.Observable
import io.reactivex.functions.Consumer

class LocalSettings(context: Context) : SettingsDataSource {

    private val preferences = RxSharedPreferences.create(context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE))
    private val nuanceLanguage = preferences.getString("nuanceLanguage", "yue-CHN")
    private val voiceSpdDefaultValues: Map<String, Int> = mapOf(
            "yue-CHN" to 4,
            "eng-USA" to 5,
            "vie-VNM" to 10
    )
    private val voicePrefDefaultValues: Map<String, Int> = mapOf(
            "yue-CHN" to 4,
            "eng-USA" to 0,
            "vie-VNM" to 2
    )

    override fun getNuanceLanguage(): Observable<String> {
        return nuanceLanguage.asObservable()
    }

    override fun setNuanceLanguage(): Consumer<in String> {
        return nuanceLanguage.asConsumer()
    }

    override fun getVoicePref(): Observable<Int> {
        return nuanceLanguage.asObservable().flatMap {
            preferences.getInteger("voicePref-$it", voicePrefDefaultValues[it] ?: 0).asObservable()
        }
    }

    override fun setVoicePref(): Consumer<in Int> {
        return Consumer {
            preferences.getInteger("voicePref-${nuanceLanguage.get()}").set(it)
        }
//        return preferences.getInteger("voicePref", 4).asConsumer()
    }

    override fun getVoicePref(lang: String): Observable<Int> {
        return preferences.getInteger("voicePref-$lang", voicePrefDefaultValues[lang]
                ?: 0).asObservable()
    }

    override fun getVoiceSpd(): Observable<Int> {
        return nuanceLanguage.asObservable().flatMap {
            preferences.getInteger("voiceSpd-$it", voiceSpdDefaultValues[it] ?: 5).asObservable()
        }
    }

    override fun setVoiceSpd(): Consumer<in Int> {
        return Consumer {
            val lang = nuanceLanguage.get()
            preferences.getInteger("voiceSpd-$lang").set(it)
        }
    }

    override fun getVoiceSpd(lang: String): Observable<Int> {
        return preferences.getInteger("voiceSpd-$lang", voiceSpdDefaultValues[lang]
                ?: 5).asObservable()
    }

    override fun getPinyin(): Observable<Boolean> {
        return preferences.getBoolean("pinyin", true).asObservable()
    }

    override fun setPinyin(): Consumer<in Boolean> {
        return preferences.getBoolean("pinyin").asConsumer()
    }

    override fun getHandFree(): Observable<Boolean> {
        return preferences.getBoolean("hand-free").asObservable()
    }

    override fun setHandFree(): Consumer<in Boolean> {
        return preferences.getBoolean("hand-free").asConsumer()
    }

}