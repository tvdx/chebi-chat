package com.tinhvan.chebichat.settings

import io.reactivex.Observable
import io.reactivex.functions.Consumer

interface SettingsDataSource {
    fun getNuanceLanguage() : Observable<String>
    fun setNuanceLanguage() : Consumer<in String>

    fun getVoicePref(lang: String) : Observable<Int>
    fun getVoicePref() : Observable<Int>
    fun setVoicePref() : Consumer<in Int>

    fun getVoiceSpd(lang: String) : Observable<Int>
    fun getVoiceSpd() : Observable<Int>
    fun setVoiceSpd() : Consumer<in Int>

    fun getPinyin(): Observable<Boolean>
    fun setPinyin(): Consumer<in Boolean>

    fun getHandFree(): Observable<Boolean>
    fun setHandFree(): Consumer<in Boolean>
}