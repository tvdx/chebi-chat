package com.tinhvan.chebichat.settings

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.firebase.auth.FirebaseAuth
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.selected
import com.jakewharton.rxbinding2.widget.*
import com.tinhvan.chebichat.R
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.dialog_settings.*

class SettingsDialogFragment : DialogFragment() {
    private val settingsDataSource: SettingsDataSource by lazy {
        LocalSettings(context!!)
    }
    val btnSignInClick = PublishSubject.create<Unit>()!!
    private val auth: FirebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }
    private val disposables = ArrayList<Disposable>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_settings, container, true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (auth.currentUser != null && !auth.currentUser!!.isAnonymous) {
            btnSignIn.text = "Đăng xuất"
        } else {
            btnSignIn.text = "Đăng nhập"
        }
        btnSignIn.clicks().subscribe(btnSignInClick)

        initNuanceLanguages()
        initVoicePref()
        initVoiceSpd()
        initPinyin()
        initHandFree()

        btnSave.clicks().subscribe {
            dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposables.forEach {
            it.dispose()
        }
        disposables.clear()
    }

    private fun initVoiceSpd() {
        disposables.add(settingsDataSource.getNuanceLanguage().subscribe {
            if (it == "yue-CHN") {
                seekBarVoiceSpd.max = 15
            } else {
                seekBarVoiceSpd.max = 18
            }
        })
        settingsDataSource.getVoiceSpd()
                .also {
                    disposables.add(it.subscribe(seekBarVoiceSpd.progress()))
                    disposables.add(it.map { "Tốc độ đọc: $it" }
                            .subscribe(tvLabelVoiceSpd.text()))
                }
        seekBarVoiceSpd
                .userChanges()
                .share()
                .also {
                    disposables.add(it.subscribe(settingsDataSource.setVoiceSpd()))
                    disposables.add(it.map { "Tốc độ đọc: $it" }
                            .subscribe(tvLabelVoiceSpd.text()))
                }
    }

    private fun initVoicePref() {
        disposables.add(settingsDataSource.getNuanceLanguage().subscribe({
            val voiceArrayId = when (it) {
                "yue-CHN" -> R.array.voice_pref_chinese
                "eng-USA" -> R.array.voice_pref_english
                else -> R.array.voice_pref_vietnamese
            }
            val voiceData = ArrayAdapter(context, R.layout.item_spinner_language, R.id.tv_language, resources.getStringArray(voiceArrayId))
            voiceData.setDropDownViewResource(R.layout.item_spinner_language)

            spinnerVoicePref.adapter = voiceData
        }, { Log.e("NguyenNK", it.message, it) }))

        disposables.add(settingsDataSource.getVoicePref()
                .filter { it < spinnerVoicePref.adapter.count }
                .subscribe(spinnerVoicePref.selection()))
        spinnerVoicePref.itemSelections()
                .subscribe(settingsDataSource.setVoicePref())
    }

    private fun initNuanceLanguages() {
        val spinner = spinnerLanguage
        val languageData = ArrayAdapter(context, R.layout.item_spinner_language, R.id.tv_language, resources.getStringArray(R.array.languages))
        languageData.setDropDownViewResource(R.layout.item_spinner_language)

        spinner.adapter = languageData
        disposables.add(settingsDataSource.getNuanceLanguage()
                .map {
                    when (it) {
                        "yue-CHN" -> 1
                        "eng-USA" -> 2
                        else -> 0
                    }
                }
                .subscribe(spinner.selection()))
        spinner.itemSelections()
                .map {
                    when (it) {
                        1 -> "yue-CHN"
                        2 -> "eng-USA"
                        else -> "vie-VNM"
                    }
                }
                .subscribe(settingsDataSource.setNuanceLanguage())
    }

    private fun initPinyin() {
        settingsDataSource.getPinyin().subscribe(swPinyin.checked())
        swPinyin.checkedChanges().subscribe(settingsDataSource.setPinyin())
    }

    private fun initHandFree() {
        settingsDataSource.getHandFree().subscribe(swHandFree.checked())
        swHandFree.checkedChanges().subscribe(settingsDataSource.setHandFree())
    }
}