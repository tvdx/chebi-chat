package com.tinhvan.chebichat.viewholder

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import com.facebook.ads.AdChoicesView
import com.facebook.ads.NativeAd
import com.tinhvan.chebichat.botchat.AdChatMessage
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_fb_ad_message.*
import kotlinx.android.synthetic.main.native_ad_layout.*


class AdViewHolder(override val containerView: View?) : BaseViewHolder<AdChatMessage>(containerView), LayoutContainer {
    override fun bindData(position: Int, data: AdChatMessage) {
        try {
            inflateAd(data.nativeAd!!)
        } catch (ex: Exception) {
            Log.e("NguyenNK", "AdViewHolder bind ad error: ${ex.message}")
        }
    }

    private fun inflateAd(nativeAd: NativeAd) {

        nativeAd.unregisterView()

        // Add the Ad view into the ad container.
        val nativeAdContainer = layoutContent
        val inflater = LayoutInflater.from(itemView.context)
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.

        // Add the AdChoices icon
        val adChoicesContainer = ad_choices_container
        val adChoicesView = AdChoicesView(itemView.context, nativeAd, true)
        adChoicesContainer.addView(adChoicesView, 0)

        // Create native UI using the ad metadata.
        val nativeAdIcon = native_ad_icon
        val nativeAdTitle = native_ad_title
        val nativeAdMedia = native_ad_media
        val nativeAdSocialContext = native_ad_social_context
        val nativeAdBody = native_ad_body
        val sponsoredLabel = native_ad_sponsored_label
        val nativeAdCallToAction = native_ad_call_to_action

        // Set the Text.
        nativeAdTitle.text = nativeAd.advertiserName
        nativeAdBody.text = nativeAd.adBodyText
        nativeAdSocialContext.text = nativeAd.adSocialContext
        nativeAdCallToAction.visibility = if (nativeAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE
        nativeAdCallToAction.text = nativeAd.adCallToAction
        sponsoredLabel.text = nativeAd.sponsoredTranslation

        // Create a list of clickable views
        val clickableViews = ArrayList<View>()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView,
                nativeAdMedia,
                nativeAdIcon,
                clickableViews)
    }
}
