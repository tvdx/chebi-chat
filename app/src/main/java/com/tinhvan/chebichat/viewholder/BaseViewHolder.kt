package com.tinhvan.chebichat.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.tinhvan.chebichat.botchat.ChatMessage
import kotlinx.android.extensions.LayoutContainer

abstract class BaseViewHolder<T: ChatMessage>(itemView: View?) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    abstract fun bindData(position: Int, data: T)
}