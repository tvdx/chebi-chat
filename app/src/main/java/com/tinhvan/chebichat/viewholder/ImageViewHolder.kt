package com.tinhvan.chebichat.viewholder

import android.view.View
import com.bumptech.glide.request.RequestOptions
import com.tinhvan.chebichat.GlideApp
import com.tinhvan.chebichat.botchat.ImageChatMessage
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_image_message.view.*

class ImageViewHolder(override val containerView: View?) : BaseViewHolder<ImageChatMessage>(containerView), LayoutContainer {
    override fun bindData(position: Int, data: ImageChatMessage) {
        GlideApp.with(itemView)
                .load(data.source)
                .apply(RequestOptions.fitCenterTransform())
                .into(itemView.imvImage)
    }
}