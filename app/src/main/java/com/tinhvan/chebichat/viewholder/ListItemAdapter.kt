package com.tinhvan.chebichat.viewholder

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.tinhvan.chebichat.GlideApp
import com.tinhvan.chebichat.R
import com.tinhvan.chebichat.core.bot.Item
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_general.view.*

class ListItemAdapter(val data: List<Item>) : RecyclerView.Adapter<ListItemAdapter.ViewHolder>() {
    val newsItemClick = PublishSubject.create<Item>()!!
    val newsIconClick = PublishSubject.create<Item>()!!
    private val disposables = ArrayList<Disposable>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return LayoutInflater.from(parent.context).inflate(R.layout.item_general, parent, false).run {
            ViewHolder(this)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        data[position].apply {
            holder.itemView.tvTitle.text = this.name
            holder.itemView.tvDescription.text = this.info
            GlideApp.with(holder.itemView.context)
                    .load(if (this.icon?.startsWith("http")!!) this.icon else "http:" + this.icon)
                    .circleCrop()
                    .into(holder.itemView.imvThumbnail)

            holder.itemView.clicks()
                    .map { this }
                    .subscribe(newsItemClick::onNext).also {
                        disposables.add(it)
                    }
            holder.itemView.imvThumbnail.clicks()
                    .map { this }
                    .subscribe(newsIconClick::onNext).also {
                        disposables.add(it)
                    }
        }
    }

    class ViewHolder(override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer
}