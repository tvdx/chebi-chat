package com.tinhvan.chebichat.viewholder

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.jakewharton.rxbinding2.view.visibility
import com.tinhvan.chebichat.botchat.ListChatMessage
import com.tinhvan.chebichat.core.bot.Item
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_list_message.*
import kotlinx.android.synthetic.main.item_list_message.view.*

class ListItemViewHolder(override val containerView: View?) : BaseViewHolder<ListChatMessage>(containerView), LayoutContainer {
    val newsItemClick = PublishSubject.create<Item>()!!
    val newsIconClick = PublishSubject.create<Item>()!!

    init {
        itemView.rvNews.also {
            it.layoutManager = LinearLayoutManager(itemView.context).apply {
                initialPrefetchItemCount = 1
            }
        }
    }

    override fun bindData(position: Int, data: ListChatMessage) {
        val adapter: ListItemAdapter = ListItemAdapter(data.items as List<Item>).also {
            it.newsItemClick.subscribe(newsItemClick)
            it.newsIconClick.subscribe(newsIconClick)
        }

        rvNews.swapAdapter(adapter, false)
//        itemView.rvNews.adapter = adapter

        Observable.just(!data.nextPage.isNullOrBlank()).subscribe(btnNextItems.visibility(visibilityWhenFalse = View.GONE))
    }
}