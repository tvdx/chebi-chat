package com.tinhvan.chebichat.viewholder

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.tinhvan.chebichat.botchat.ListLyricChatMessage
import com.tinhvan.chebichat.core.bot.LyricItem
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_lyric_message.*

class ListLyricViewHolder(override val containerView: View) : BaseViewHolder<ListLyricChatMessage>(containerView), LayoutContainer {
    val lyricItemTitleClick = PublishSubject.create<Pair<LyricItem, (LyricItem) -> Unit>>()!!
    init {
        rvLyrics.layoutManager = LinearLayoutManager(itemView.context).apply {
            initialPrefetchItemCount = 1
        }
    }

    override fun bindData(position: Int, data: ListLyricChatMessage) {
        val adapter = LyricAdapter(data.items.toMutableList(), data.showPinyin).apply {
            lyricTitleClick.subscribe(lyricItemTitleClick)
        }

        rvLyrics.swapAdapter(adapter, false)
    }

    fun bindData(position: Int, data: ListLyricChatMessage, payloads: MutableList<Any>) {
        (rvLyrics.adapter as? LyricAdapter)?.showPinyin = data.showPinyin
        rvLyrics.adapter.notifyItemRangeChanged(0, data.items.size, "PINYIN")
    }
}