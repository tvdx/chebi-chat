package com.tinhvan.chebichat.viewholder

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.tinhvan.chebichat.botchat.ListChatMessage
import com.tinhvan.chebichat.core.maps.PlaceItem
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_place_message.*

class ListPlaceViewHolder(override val containerView: View?) : BaseViewHolder<ListChatMessage>(containerView), LayoutContainer {
    val itemClick = PublishSubject.create<PlaceItem>()!!

    init {
        rvPlaces.layoutManager = LinearLayoutManager(itemView.context).apply {
            initialPrefetchItemCount = 1
        }
    }
    override fun bindData(position: Int, data: ListChatMessage) {
        val adapter = PlaceAdapter(data.items as List<PlaceItem>).also {
            it.itemClick.subscribe(itemClick)
        }

        rvPlaces.swapAdapter(adapter, false)
    }

}
