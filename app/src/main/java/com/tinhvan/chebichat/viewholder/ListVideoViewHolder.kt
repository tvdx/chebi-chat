package com.tinhvan.chebichat.viewholder

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.jakewharton.rxbinding2.view.visibility
import com.tinhvan.chebichat.botchat.ListChatMessage
import com.tinhvan.chebichat.core.bot.VideoItem
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_video_message.*
import kotlinx.android.synthetic.main.item_video_message.view.*

class ListVideoViewHolder(override val containerView: View?) : BaseViewHolder<ListChatMessage>(containerView) {
    val videoItemClick: PublishSubject<VideoItem> = PublishSubject.create()

    override fun bindData(position: Int, data: ListChatMessage) {
        val llm = LinearLayoutManager(itemView.context)
        val adapter = VideoAdapter(data.items as List<VideoItem>)
        adapter.itemClick.subscribe(videoItemClick)
        itemView.rvVideoList.layoutManager = llm
        itemView.rvVideoList.adapter = adapter

        Observable.just(!data.nextPage.isNullOrBlank()).subscribe(btnNextVideos.visibility(visibilityWhenFalse = View.GONE))
    }
}