package com.tinhvan.chebichat.viewholder

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.tinhvan.chebichat.botchat.ListChatMessage
import com.tinhvan.chebichat.core.bot.WikiItem
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_wiki_message.*

class ListWikiViewHolder(override val containerView: View) : BaseViewHolder<ListChatMessage>(containerView), LayoutContainer {
    val itemDetailsClick = PublishSubject.create<WikiItem>()!!
    val itemSpeakClick = PublishSubject.create<Pair<WikiItem, (WikiItem) -> Unit>>()!!
    val itemTranslateClick = PublishSubject.create<Pair<WikiItem, (WikiItem, String) -> Unit>>()!!

    init {
        rvWikiArticles.layoutManager = LinearLayoutManager(itemView.context).apply {
            initialPrefetchItemCount = 1
        }
    }

    override fun bindData(position: Int, data: ListChatMessage) {
        val adapter = WikiAdapter(data.items.toMutableList() as MutableList<WikiItem>).apply {
            detailsClick.subscribe(itemDetailsClick)
            speakClick.subscribe(itemSpeakClick)
            translateClick
                    .subscribe(itemTranslateClick)
        }

        rvWikiArticles.swapAdapter(adapter, false)
    }
}