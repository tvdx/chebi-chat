package com.tinhvan.chebichat.viewholder

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.visibility
import com.tinhvan.chebichat.R
import com.tinhvan.chebichat.core.bot.LyricItem
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_lyric.*

class LyricAdapter(var data: MutableList<LyricItem>,
                   var showPinyin: Boolean = false) : RecyclerView.Adapter<LyricAdapter.ViewHolder>() {
    val lyricTitleClick = PublishSubject.create<Pair<LyricItem, (LyricItem) -> Unit>>()!!
    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bindData(position, item, showPinyin)
        val clicks = holder.tvLyricTitle.clicks()
        clicks
                .map { holder.layoutLyric.visibility().accept(holder.layoutLyric.visibility != View.VISIBLE) }
                .filter { data[position].lyric == null }.map {
                    Pair(item, { lyricItem: LyricItem ->
                        data[position] = lyricItem
                        notifyItemChanged(position, "LYRIC")
                    })
                }.subscribe(lyricTitleClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
            return
        }

        holder.bindData(position, data[position], payloads, showPinyin)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_lyric, parent, false))
    }

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bindData(position: Int, data: LyricItem, showPinyin: Boolean) {
            Observable.just(position == 0).subscribe(layoutLyric.visibility())
            tvLyricTitle.text = data.trackName
            tvLyric.text = data.lyric
            data.pinyinDisplay?.let {
                tvFurigana.setFuriganaText(it, true)
            }
            setShowPinyin(showPinyin, data)
        }

        fun bindData(position: Int, data: LyricItem, payloads: MutableList<Any>, showPinyin: Boolean) {
            when (payloads[0] as? String) {
                "LYRIC" -> {
                    Observable.just(data.loading).subscribe(progressLoading.visibility())
                    tvLyric.text = data.lyric
                    data.pinyinDisplay?.let {
                        tvFurigana.setFuriganaText(it, true)
                    }
                    setShowPinyin(showPinyin, data)
                }
                "PINYIN" -> {
                    setShowPinyin(showPinyin, data)
                }
            }
        }

        private fun setShowPinyin(showPinyin: Boolean, data: LyricItem) {
            if (showPinyin && data.pinyinDisplay != null) {
                tvLyric.visibility = View.GONE
                tvFurigana.visibility = View.VISIBLE
            } else {
                tvLyric.visibility = View.VISIBLE
                tvFurigana.visibility = View.GONE
            }
        }
    }
}