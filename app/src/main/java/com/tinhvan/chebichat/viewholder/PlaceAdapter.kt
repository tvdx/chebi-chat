package com.tinhvan.chebichat.viewholder

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.tinhvan.chebichat.GlideApp
import com.tinhvan.chebichat.R
import com.tinhvan.chebichat.core.maps.PlaceItem
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_place.*

class PlaceAdapter(val data: List<PlaceItem>) : RecyclerView.Adapter<PlaceAdapter.ViewHolder>() {
    val itemClick = PublishSubject.create<PlaceItem>()!!

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_place, parent,false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bindData(position, item)
        holder.itemView.clicks()
                .map {
                    item
                }
                .subscribeWith(itemClick)
    }

    class ViewHolder(override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        @SuppressLint("SetTextI18n")
        fun bindData(position: Int, item: PlaceItem) {
            tvName.text = item.name
            tvAddress.text = "${item.address} (Rating: ${item.rating})"
            tvDistance.text = "Khoảng cách: ${item.distance}m"
            GlideApp.with(itemView)
                    .load(item.thumbnail)
                    .circleCrop()
                    .into(imvThumbnail)
        }
    }
}
