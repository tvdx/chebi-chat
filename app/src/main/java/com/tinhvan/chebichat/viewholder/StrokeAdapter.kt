package com.tinhvan.chebichat.viewholder

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.request.ImageRequest
import com.jakewharton.rxbinding2.view.clicks
import com.tinhvan.chebichat.GlideRequests
import com.tinhvan.chebichat.R
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_stroke.view.*

class StrokeAdapter(private val urls: List<String>) : RecyclerView.Adapter<StrokeAdapter.ViewHolder>() {
    val itemClick = PublishSubject.create<String>()!!

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_stroke, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return urls.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = urls[position]

        holder.itemView.image
                .controller = Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequest.fromUri(item))
                .setAutoPlayAnimations(true)
                .build()
        holder.itemView.image.clicks()
                .map { item }
                .subscribe(itemClick)
    }

    class ViewHolder(override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer
}