package com.tinhvan.chebichat.viewholder

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.tinhvan.chebichat.botchat.StrokeChatMessage
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_stroke_message.view.*

class StrokeViewHolder(override val containerView: View?) : BaseViewHolder<StrokeChatMessage>(containerView), LayoutContainer {
    val itemClick = PublishSubject.create<String>()!!

    override fun bindData(position: Int, data: StrokeChatMessage) {
        val adapter = StrokeAdapter(data.urls).also {
            it.itemClick.subscribe(itemClick)
        }
        val llm = LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)

        itemView.rvImages.apply {
            setAdapter(adapter)
            layoutManager = llm
        }
    }
}