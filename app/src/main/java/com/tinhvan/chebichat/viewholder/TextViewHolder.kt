package com.tinhvan.chebichat.viewholder

import android.graphics.drawable.Drawable
import android.text.Html
import android.view.View
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.visibility
import com.tinhvan.chebichat.*
import com.tinhvan.chebichat.botchat.TextChatMessage
import com.tinhvan.chebichat.botchat.TranslateStatus
import io.reactivex.Observable
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_text_message_me.*
import kotlinx.android.synthetic.main.item_text_message_other.view.*

class TextViewHolder(override val containerView: View?) : BaseViewHolder<TextChatMessage>(containerView), LayoutContainer {
    override fun bindData(position: Int, data: TextChatMessage) {
        displayText(data)
        if (data.mine) {
            imvAvatarMine.visibility = View.GONE
            data.avatar?.let {
                GlideApp.with(itemView)
                        .load(it)
                        .apply(RequestOptions.circleCropTransform())
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                imvAvatarMine.visibility = View.VISIBLE
                                return false
                            }
                        })
                        .into(imvAvatarMine)
            }
        }
        itemView.layoutActions.visibility().accept(data.showingActions)
        itemView.btnToggleActions.clicks()
                .map {
                    val showAction = itemView.layoutActions.visibility != View.VISIBLE
                    data.showingActions = showAction
                    showAction
                }
                .subscribe(itemView.layoutActions.visibility(visibilityWhenFalse = View.GONE))
        itemView.progressTranslate.isIndeterminate = true
        itemView.progressSpeak.isIndeterminate = true
        Observable.just(data.loadingSpeak)
                .subscribe(itemView.progressSpeak.visibility(View.INVISIBLE))

        when (data.translated) {
            TranslateStatus.NON_TRANSLATED -> {
                itemView.btnTranslate.isEnabled = true
                itemView.btnTranslate.alpha = 1.0f
                itemView.progressTranslate.visibility = View.INVISIBLE
            }
            TranslateStatus.TRANSLATING -> {
                itemView.btnTranslate.isEnabled = false
                itemView.btnTranslate.alpha = 1.0f
                itemView.progressTranslate.visibility = View.VISIBLE
            }
            TranslateStatus.TRANSLATED -> {
                itemView.btnTranslate.isEnabled = false
                itemView.btnTranslate.alpha = 0.5f
                itemView.progressTranslate.visibility = View.INVISIBLE
            }
        }
    }

    fun bindData(data: TextChatMessage, payload: MutableList<Any>) {
        when (payload[0] as? String) {
            "SPEAK" -> Observable.just(data.loadingSpeak)
                    .subscribe(itemView.progressSpeak.visibility(View.INVISIBLE))
            "TRANSLATE" -> Observable.just(data.translated).share().let {
                it.map { it == TranslateStatus.NON_TRANSLATED }.subscribe { itemView.btnTranslate.isEnabled = it }
                it.map { if (it == TranslateStatus.TRANSLATED) 0.5f else 1f }.subscribe { itemView.btnTranslate.alpha = it }
                it.map { it == TranslateStatus.TRANSLATING }.subscribe(itemView.progressTranslate.visibility(View.INVISIBLE))
            }
            "PINYIN" -> {
                displayText(data)
            }
        }
    }

    private fun displayText(data: TextChatMessage) {
        if (data.displayText.isNullOrEmpty() || !data.showPinyin) {
            itemView.tvMessage.text = Html.fromHtml(data.textMessage)
            itemView.tvFurigana.visibility = View.GONE
            itemView.tvMessage.visibility = View.VISIBLE
        } else {
//            val res = data.pinyin!!.split(" ")
//                    .zip(data.textMessage.asIterable()) { t1: String, t2: Char ->
//                        val space = " ".repeat(t1.length / 2)
//                        "$space<ruby>$t2<rt>$t1</rt></ruby>$space"
//                    }
//                    .reduce { acc, s -> "$acc $s" }
//            Log.d("NguyenNK", "Pinyin: ${data.pinyin} - Message: ${data.message}")
            itemView.tvFurigana.setFuriganaText(data.displayText!!, true)
            itemView.tvFurigana.visibility = View.VISIBLE
            itemView.tvMessage.visibility = View.GONE
        }
    }
}