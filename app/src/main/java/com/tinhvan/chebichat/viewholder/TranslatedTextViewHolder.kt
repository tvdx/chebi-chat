package com.tinhvan.chebichat.viewholder

import android.text.Html
import android.view.View
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.visibility
import com.tinhvan.chebichat.botchat.TranslatedTextChatMessage
import io.reactivex.Observable
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_text_message_other.view.*

class TranslatedTextViewHolder(override val containerView: View) : BaseViewHolder<TranslatedTextChatMessage>(containerView), LayoutContainer {
    override fun bindData(position: Int, data: TranslatedTextChatMessage) {
        displayText(data)
        itemView.btnTranslate.visibility = View.GONE
        itemView.progressTranslate.visibility = View.GONE

        Observable.just(data.loadingSpeak)
                .subscribe(itemView.progressSpeak.visibility(View.INVISIBLE))

        itemView.btnToggleActions.clicks()
                .map { itemView.layoutActions.visibility != View.VISIBLE }
                .subscribe(itemView.layoutActions.visibility(visibilityWhenFalse = View.GONE))
    }

    fun bindData(data: TranslatedTextChatMessage, payload: MutableList<Any>) {
        when (payload[0] as String) {
            "SPEAK" -> Observable.just(data.loadingSpeak)
                    .subscribe(itemView.progressSpeak.visibility(View.INVISIBLE))
            "PINYIN" -> {
                displayText(data)
            }
        }
    }

    private fun displayText(data: TranslatedTextChatMessage) {
        if (data.displayText.isNullOrEmpty() || !data.showPinyin) {
            itemView.tvMessage.text = Html.fromHtml(data.textMessage)
            itemView.tvFurigana.visibility = View.GONE
            itemView.tvMessage.visibility = View.VISIBLE
        } else {
//            val res = data.pinyin!!.split(" ").zip(data.textMessage.asIterable()) { t1: String, t2: Char -> "<ruby>$t2<rt>$t1</rt></ruby>" }.reduce { acc, s -> "$acc $s" }
//            itemView.tvFurigana.setFuriganaText(res, true)
            itemView.tvFurigana.setFuriganaText(data.displayText!!, true)
            itemView.tvFurigana.visibility = View.VISIBLE
            itemView.tvMessage.visibility = View.GONE
        }
    }
}
