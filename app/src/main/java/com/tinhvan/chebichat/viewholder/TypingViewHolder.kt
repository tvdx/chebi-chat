package com.tinhvan.chebichat.viewholder

import android.view.View
import com.tinhvan.chebichat.GlideApp
import com.tinhvan.chebichat.botchat.TypingMessage
import kotlinx.android.extensions.LayoutContainer
import com.tinhvan.chebichat.R
import kotlinx.android.synthetic.main.item_typing_message.*

class TypingViewHolder(override val containerView: View?) : BaseViewHolder<TypingMessage>(containerView), LayoutContainer {
    override fun bindData(position: Int, data: TypingMessage) {
        GlideApp.with(itemView)
                .load(R.mipmap.gif_typing4)
                .centerCrop()
                .into(imvTyping)
    }

}