package com.tinhvan.chebichat.viewholder

import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import com.tinhvan.chebichat.botchat.UrlChatMessage
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_url_message.view.*

class UrlViewHolder(override val containerView: View) : BaseViewHolder<UrlChatMessage>(containerView), LayoutContainer {
    override fun bindData(position: Int, data: UrlChatMessage) {
        SpannableString(data.text).also {
            it.setSpan(UnderlineSpan(), 0, it.length, 0)
            itemView.tvMessage.text = it
        }

    }
}