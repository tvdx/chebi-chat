package com.tinhvan.chebichat.viewholder

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.request.RequestOptions
import com.jakewharton.rxbinding2.view.clicks
import com.tinhvan.chebichat.GlideApp
import com.tinhvan.chebichat.R
import com.tinhvan.chebichat.core.bot.VideoItem
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_video.view.*

class VideoAdapter(private val videoList: List<VideoItem>) : RecyclerView.Adapter<VideoAdapter.ItemVideoViewHolder>() {
    val itemClick = PublishSubject.create<VideoItem>()!!

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemVideoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_video, parent, false)
        return ItemVideoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return videoList.size
    }

    override fun onBindViewHolder(holder: ItemVideoViewHolder, position: Int) {
        val item = videoList[position]
        holder.bindData(item)
        holder.itemView.clicks()
                .map { item }
                .subscribe(itemClick)
    }

    class ItemVideoViewHolder(override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        private val dateTimeRegex = Regex("[HMS]")

        fun bindData(data: VideoItem) {
            GlideApp.with(itemView)
                    .load(data.thumbnail)
                    .apply(RequestOptions.circleCropTransform())
                    .into(itemView.imvThumbnail)

            itemView.tvTitle.text = data.title
            itemView.tvTime.text = data.duration?.substring(2, data.duration!!.length - 1)?.replace(dateTimeRegex, ":")
            itemView.tvLike.text = data.likeCount
            itemView.tvViewCount.text = data.viewCount
        }
    }
}