package com.tinhvan.chebichat.viewholder

import android.support.v7.widget.RecyclerView
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.visibility
import com.jakewharton.rxbinding2.widget.text
import com.tinhvan.chebichat.R
import com.tinhvan.chebichat.core.bot.WikiItem
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_wiki.*
import kotlinx.android.synthetic.main.item_wiki.view.*

class WikiAdapter(var data: MutableList<WikiItem>) : RecyclerView.Adapter<WikiAdapter.ViewHolder>() {
    val detailsClick = PublishSubject.create<WikiItem>()!!
    val speakClick = PublishSubject.create<Pair<WikiItem, (WikiItem) -> Unit>>()!!
    val translateClick = PublishSubject.create<Pair<WikiItem, (WikiItem, String) -> Unit>>()!!

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bindData(position, item)
        holder.itemView.tvDetails.clicks()
                .map { item }
                .subscribe(detailsClick)
        holder.itemView.btnSpeak.clicks()
                .map {
                    data
                            .forEachIndexed { index, item ->
                                if (item.loadingSpeak) {
                                    data[index] = item.copy(loadingSpeak = false)
                                    notifyItemChanged(index, "SPEAK")
                                }
                            }
                    Pair(data[position], { wikiItem: WikiItem ->
                        data[position] = wikiItem
                        notifyItemChanged(position, "SPEAK")
                    })
                }
                .subscribe(speakClick)
        holder.itemView.btnWikiTranslate.clicks()
                .map {
                    data
                            .forEachIndexed { index, item ->
                                if (item.loadingTranslate) {
                                    data[index] = item.copy(loadingTranslate = false)
                                    notifyItemChanged(index, "TRANSLATE_STATUS")
                                }
                            }

                    Pair(data[position], { wikiItem: WikiItem, payload: String ->
                        data[position] = wikiItem
                        notifyItemChanged(position, payload)
                    })
                }
                .subscribe(translateClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
            return
        }

        holder.bindData(position, data[position], payloads)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_wiki, parent, false))
    }

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        init {
            tvTopic.clicks().map {
                layoutActions.visibility != View.VISIBLE
            }.share().subscribe {
                Log.d("NguyenNK", "Wiki visibility: $it")
                layoutActions.visibility().accept(it)
            }
        }

        fun bindData(position: Int, data: WikiItem) {
            Observable.just(position == 0).subscribe(layoutActions.visibility())
            tvTopic.text = data.topic
            Observable.just(data.translatedDesc == tvDescription.text)
                    .map {
                        if (it) data.description else data.translatedDesc ?: data.description
                    }
                    .subscribe(tvDescription.text())
            SpannableString(tvDetails.text).also {
                it.setSpan(UnderlineSpan(), 0, tvDetails.text.length, 0)
                tvDetails.text = it
            }
        }

        fun bindData(position: Int, data: WikiItem, payloads: MutableList<Any>) {
            when (payloads[0] as? String) {
                "TRANSLATE" -> {
                    Observable.just(data.translatedDesc == tvDescription.text)
                            .map {
                                if (it) data.description else data.translatedDesc
                                        ?: data.description
                            }
                            .subscribe(tvDescription.text())
                    Observable.just(data.loadingTranslate).subscribe(progressTranslate.visibility(View.INVISIBLE))
                }
                "TRANSLATE_STATUS" -> {
                    Observable.just(data.loadingTranslate).subscribe(progressTranslate.visibility(View.INVISIBLE))
                }
                "SPEAK" -> {
                    Observable.just(data.loadingSpeak).subscribe(progressSpeak.visibility(View.INVISIBLE))
                }

            }
        }
    }
}